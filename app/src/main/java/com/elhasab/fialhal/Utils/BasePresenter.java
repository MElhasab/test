package com.elhasab.fialhal.Utils;

import android.content.Context;
import android.widget.ImageView;

import static com.elhasab.fialhal.Utils.ParentClass.LoadImageWithPicasso;
import static com.elhasab.fialhal.Utils.ParentClass.dismissFlipDialog;
import static com.elhasab.fialhal.Utils.ParentClass.handleException;
import static com.elhasab.fialhal.Utils.ParentClass.makeErrorToast;
import static com.elhasab.fialhal.Utils.ParentClass.makeSuccessToast;
import static com.elhasab.fialhal.Utils.ParentClass.showFlipDialog;


public class BasePresenter {


    public void startLoading() {
        showFlipDialog();
    }

    public void stopLoading() {
        dismissFlipDialog();
    }

    public void errorToast(Context context, String msg) {
        makeErrorToast(context, msg);
    }

    public void successToast(Context context, String msg) {
        makeSuccessToast(context, msg);
    }

    public void handleApiException(Context context, Throwable t) {
        handleException(context, t);
    }

    public void loadImage(String url, Context context, ImageView imageView) {
        LoadImageWithPicasso(url, context, imageView);
    }
}
