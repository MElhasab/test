
package com.elhasab.fialhal.Utils.MenuListFragment.View;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.AboutApp.AboutAppActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.ChatHistory.View.ChatHistoryActivity;
import com.elhasab.fialhal.User.ContactUs.View.ContactUsActivity;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.User.PrivacyAndPolicy.View.PrivacyAndPolicyActivity;
import com.elhasab.fialhal.User.Profile.View.ProfileActivity;
import com.elhasab.fialhal.User.UserOrders.View.RequestsActivity;
import com.elhasab.fialhal.User.Wallet.View.WalletActivity;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.MenuListFragment.Presenter.MenuListFragmentPresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;
import com.google.android.material.navigation.NavigationView;
//import com.elhasab.fialhal.AboutApp.View.AboutAppActivity;
//import com.headshot.elcindrella.Authentication.Login.View.LoginActivity;
//import com.headshot.elcindrella.ContactUS.View.ContactUsActivity;
//import com.headshot.elcindrella.Provider.CalculateCommisionActivity.View.CalculateCommisionActivity;
//import com.headshot.elcindrella.Provider.HomeProviderActivity.View.HomeProviderActivity;
//import com.headshot.elcindrella.Provider.ProviderProfile.View.ProviderProfileActivity;
//import com.headshot.elcindrella.Provider.ProviderSettingsActivity.View.ProviderSettingsActivity;
//import com.headshot.elcindrella.Provider.WalletActivity.View.WalletActivity;
//import com.headshot.elcindrella.R;
//import com.headshot.elcindrella.User.ChatHistory.View.ChatHistoryActivity;
//import com.headshot.elcindrella.User.FilterResults.View.FilterResultsActivity;
//import com.headshot.elcindrella.User.HomeUser.View.HomeUserActivity;
//import com.headshot.elcindrella.User.UserFavourites.View.UserFavouritesActivity;
//import com.headshot.elcindrella.User.UserNotification.View.UserNotificationsActivity;
//import com.elhasab.fialhal.User.UserOffers.View.UserOffersActivity;
//import com.elhasab.fialhal.User.UserProfile.View.UserProfileActivity;
//import com.elhasab.fialhal.User.UserReservation.View.UserReservationsActivity;
//import com.elhasab.fialhal.Utils.Constants;
//import com.elhasab.fialhal.Utils.MenuListFragment.Presenter.MenuListFragmentPresenter;
//import com.elhasab.fialhal.Utils.ParentClass;
//import com.elhasab.fialhal.Utils.SharedPrefManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mxn on 2016/12/13.
 * MenuListFragment
 */

public class MenuListFragment extends Fragment {
    @BindView(R.id.ivLogo)
    ImageView ivLogo;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.tvPoints)
    TextView tvPoints;
    Dialog dialog_insted_of_toast;
    TextView tvDone;
    String deviceToken = "tokenToBeChanged";

    MenuListFragmentPresenter menuListFragmentPresenter;

    SharedPrefManager sharedPrefManager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_list,container,
                false);
        ButterKnife.bind(this,view);
        initUi();

        NavigationView vNavigation = (NavigationView) view.findViewById(R.id.nav_view);
        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                Toast.makeText(getActivity(), menuItem.getTitle(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return view;
    }

    private void initUi() {
        sharedPrefManager = new SharedPrefManager(getContext());
        SharedPreferences prefs = getActivity().getSharedPreferences(Constants.mobileToken,MODE_PRIVATE);
        if (!prefs.getString("m_token","").equals("")) {
            deviceToken = prefs.getString("m_token","");
        }
        menuListFragmentPresenter = new MenuListFragmentPresenter(getActivity(),sharedPrefManager);

        if (sharedPrefManager.getUserDate().getImage().equals("")) {
            ivLogo.setImageResource(R.drawable.splash);
        } else {
            ParentClass.LoadImageWithPicasso(sharedPrefManager.getUserDate().getImage(),getActivity(),ivLogo);
        }
        tvHeader.setText(sharedPrefManager.getUserDate().getUsername());
        if (sharedPrefManager.getUserDate().getWallet().equals("")) {
            tvPoints.setText("0" + " " + getString(R.string.point));
        } else {
            tvPoints.setText(sharedPrefManager.getUserDate().getWallet() + " " + getString(R.string.point));
        }


    }
    @OnClick(R.id.rlNotifications)
    void ivNotification() {
        Intent intent = new Intent(getActivity(), HomeUserActivity.class);
        startActivity(intent);
        Bungee.split(getActivity());
    }

    @OnClick(R.id.rlHome)
    void relativeHome() {
        if (sharedPrefManager.getUserDate().getType().equals("client")) {
            Intent intent = new Intent(getActivity(),HomeUserActivity.class);
            startActivity(intent);
            Bungee.split(getActivity());
        } else {
            Intent intent = new Intent(getActivity(), HomeWorkerActivity.class);
            startActivity(intent);
            Bungee.split(getActivity());
        }
    }

    @OnClick(R.id.rlProfile)
    void relativeProfile() {
        if (sharedPrefManager.getUserDate().getType().equals("client")) {
            Intent intent = new Intent(getActivity(), ProfileActivity.class);
            intent.putExtra("type","user");
            startActivity(intent);
            Bungee.split(getActivity());
        } else {
            Intent intent = new Intent(getActivity(),ProfileActivity.class);
            intent.putExtra("type","worker");
            startActivity(intent);
            Bungee.split(getActivity());
        }
    }

    @OnClick(R.id.rlRequests)
    void relativeRequests() {
        if (sharedPrefManager.getUserDate().getType().equals("client")) {
            Intent intent = new Intent(getActivity(),RequestsActivity.class);
            intent.putExtra("type","user");
            startActivity(intent);
            Bungee.split(getActivity());
        } else {
            Intent intent = new Intent(getActivity(), RequestsActivity.class);
            intent.putExtra("type","worker");
            startActivity(intent);
            Bungee.split(getActivity());
        }

    }

    @OnClick(R.id.rlTransactions)
    void relativeTransactions() {
        if (sharedPrefManager.getUserDate().getType().equals("client")) {
            Intent intent = new Intent(getActivity(), WalletActivity.class);
            intent.putExtra("type","user");
            startActivity(intent);
            Bungee.split(getActivity());
        } else {
            Intent intent = new Intent(getActivity(),WalletActivity.class);
            intent.putExtra("type","worker");
            startActivity(intent);
            Bungee.split(getActivity());
        }

    }

    @OnClick(R.id.rlMyMessages)
    void relativeMessages() {
        Intent intent = new Intent(getActivity(), ChatHistoryActivity.class);
        startActivity(intent);
        Bungee.split(getActivity());
    }

    @OnClick(R.id.rlAboutApp)
    void relativeAboutUs() {
        Intent intent = new Intent(getActivity(), AboutAppActivity.class);
        startActivity(intent);
        Bungee.split(getActivity());
    }

    @OnClick(R.id.rlPolicy)
    void relativePrivacyPolicy() {
        Intent intent = new Intent(getActivity(), PrivacyAndPolicyActivity.class);
        startActivity(intent);
        Bungee.split(getActivity());
    }

    @OnClick(R.id.rlContactUs)
    void relativeContactUs() {
        Intent intent = new Intent(getActivity(), ContactUsActivity.class);
        startActivity(intent);
        Bungee.split(getActivity());
    }

    @OnClick(R.id.rlLogout)
    void relativeLogout() {
        menuListFragmentPresenter.logout(deviceToken);
    }
}
