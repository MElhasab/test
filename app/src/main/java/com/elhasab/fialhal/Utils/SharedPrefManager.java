package com.elhasab.fialhal.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.elhasab.fialhal.Models.UserModel.Data;
import com.elhasab.fialhal.Models.UserModel.Data;


/**
 * Created by omarn on shadow3/5/2018.
 */

public class   SharedPrefManager {
    final static String SHARED_Setting_Data = "setting_data";
    final static String SHARED_PREF_NAME = "masari_shared";
    final static String LOGIN_STATUS = "masari_shared_login_status";
    final static String FIRST_TIME = "masari_shared_first_time";
    final static String type = "masari_shared_type";
    final static String type2 = "masari_shared_type2";
    final static String serial = "serial";
    final static String OrderId = "OrderId";
    final static String settingsCached = "settingsCached";
    final static String settingsCache = "settingsCache";
    final static String numberOfChats = "masari_sharednumberOfChats";


    Context mContext;
    Bundle bundle = new Bundle();
    Bundle args = new Bundle();
    Bundle searchArgs = new Bundle();

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
    }

    public Boolean getLoginStatus() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Boolean value = sharedPreferences.getBoolean(LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.apply();
    }

    public String getOrderId() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                OrderId, 0);
        String value = sharedPreferences.getString("OrderId", "");
        return value;
    }

    public void setOrderId(String Order) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(OrderId,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("OrderId", Order);
        editor.apply();
    }

    public Boolean isFirstTime() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Boolean value = sharedPreferences.getBoolean(FIRST_TIME, true);
        return value;
    }

    public void setFirstTime(Boolean status) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(FIRST_TIME, status);
        editor.apply();
    }


    public String getLat() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                type, 0);
        String value = sharedPreferences.getString(type, "");
        return value;
    }

    public void setLat(String lat) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(type,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(type, lat);
        editor.apply();
    }


    public String getLng() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                type2, 0);
        String value = sharedPreferences.getString(type2, "");
        return value;
    }

    public void setLng(String lng) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(type2,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(type2, lng);
        editor.apply();
    }

    public String getSerial() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                serial, 0);
        String value = sharedPreferences.getString(serial, "");
        return value;
    }

    public void setSerial(String seriall) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(serial,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(serial, seriall);
        editor.apply();
    }

    /**
     * return userModel which hold all user data
     *
     * @return user model
     */
    public Data getUserDate() {
        Data userModel = new Data();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0);
        userModel.setId(sharedPreferences.getInt("id", 0));
        userModel.setEmail(sharedPreferences.getString("email", ""));
        userModel.setUsername(sharedPreferences.getString("username", ""));
        userModel.setMobile(sharedPreferences.getString("mobile", ""));
        userModel.setCountryId(sharedPreferences.getInt("country_id", 0));
        userModel.setCountryName(sharedPreferences.getString("country_name", ""));
        userModel.setCityId(sharedPreferences.getInt("city_id", 0));
        userModel.setCityName(sharedPreferences.getString("city_name", ""));
        userModel.setStatus(sharedPreferences.getString("status", ""));
        userModel.setExperience(sharedPreferences.getString("experience", ""));
        userModel.setJob(sharedPreferences.getString("job", ""));
        userModel.setCategoryType(sharedPreferences.getString("category_type", ""));
        userModel.setLat(sharedPreferences.getString("lat", ""));
        userModel.setLng(sharedPreferences.getString("lng", ""));
        userModel.setImage(sharedPreferences.getString("image", ""));
        userModel.setToken(sharedPreferences.getString("token", ""));
        userModel.setCode(sharedPreferences.getString("code", ""));
        userModel.setType(sharedPreferences.getString("type", ""));
        userModel.setWallet(sharedPreferences.getString("wallet", ""));
        return userModel;
    }


    /**
     * saving user data to be used in profile
     *
     * @param user is the model which hold all user data
     */
    public void setUserDate(Data user) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("id", user.getId());
        editor.putString("type", user.getType());
        editor.putString("username", user.getUsername());
        editor.putString("mobile", user.getMobile());
        editor.putString("email", user.getEmail());
        editor.putString("image", user.getImage());
        editor.putString("status", user.getStatus());
        editor.putString("code", user.getCode());
        editor.putString("lat", user.getLat());
        editor.putString("lng", user.getLng());
        editor.putString("country_name", user.getCountryName());
        editor.putInt("country_id", user.getCountryId());
        editor.putString("city_name", user.getCityName());
        editor.putInt("city_id", user.getCityId());
        editor.putString("wallet", user.getWallet());
        editor.putString("token", user.getToken());
        editor.putString("experience", user.getExperience());
        editor.putString("job", user.getJob());
        editor.putString("category_type", user.getCategoryType());
        editor.apply();
    }

//    public com.codevalley.naeman.Models.SettingsModel.Data getSettings() {
//        com.codevalley.naeman.Models.SettingsModel.Data userModel = new com.codevalley.naeman.Models.SettingsModel.Data();
//        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0);
//        userModel.setAbout(sharedPreferences.getString("about", ""));
//        userModel.setOpenStatus(sharedPreferences.getString("open_status", ""));
//        userModel.setAndroidLink(sharedPreferences.getString("android_link", ""));
//        userModel.setIosLink(sharedPreferences.getString("ios_link", ""));
//        return userModel;
//    }


    /**
     * saving user data to be used in profile
     */
//    public void setSettings(com.codevalley.naeman.Models.SettingsModel.Data user) {
//        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("about", user.getAbout());
//        editor.putString("open_status", user.getOpenStatus());
//        if (user.getIosLink()==null){
//            editor.putString("ios_link","");
//
//        }else {
//            editor.putString("ios_link", user.getIosLink());
//
//        }
//        if (user.getAndroidLink()==null){
//            editor.putString("android_link", "");
//
//        }else {
//            editor.putString("android_link", user.getAndroidLink());
//
//        }
//        editor.apply();
//    }
    public void setSingleId(String singleId) {

        args.putString("singleId", singleId);
    }

    public Bundle getSingleId() {
        if (args.isEmpty()) {
        }
        return args;
    }


    public void setSettingsData(com.elhasab.fialhal.Models.SettingModel.Data settingsData) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_Setting_Data, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("about", settingsData.getAbout());
        editor.putString("policy", settingsData.getPolicy());
        editor.putString("mobile", settingsData.getMobile());
        editor.putString("facebook", settingsData.getFacebook());
        editor.putString("twitter", settingsData.getTwitter());
        editor.putString("email", settingsData.getEmail());
        editor.apply();

    }

    public com.elhasab.fialhal.Models.SettingModel.Data getSettingsData() {
        com.elhasab.fialhal.Models.SettingModel.Data data = new com.elhasab.fialhal.Models.SettingModel.Data();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_Setting_Data, 0);
        data.setAbout(sharedPreferences.getString("about", ""));
        data.setPolicy(sharedPreferences.getString("policy", ""));
        data.setMobile(sharedPreferences.getString("mobile", ""));
        data.setFacebook(sharedPreferences.getString("facebook", ""));
        data.setTwitter(sharedPreferences.getString("twitter", ""));
        data.setEmail(sharedPreferences.getString("email", ""));
        return data;

    }

    public void setSearch(String categoryId, String cityId, String type, String price, String model, String fe2a, String fe2aId, String year
            , String kilo, String capacity, String colour, Boolean New, Boolean Used, Boolean Selling, Boolean Wanted, String previousFragment, String typeId, String plateCountry, String typeOfDelivery
            , String typeGarageId, String transportationCompanies) {
        searchArgs.putString("categoryId", categoryId);
        searchArgs.putString("cityId", cityId);
        searchArgs.putString("type", type);
        searchArgs.putString("price", price);
        searchArgs.putString("model", model);
        searchArgs.putString("fe2a", fe2a);
        searchArgs.putString("fe2aId", fe2aId);
        searchArgs.putString("year", year);
        searchArgs.putString("kilo", kilo);
        searchArgs.putString("capacity", capacity);
        searchArgs.putString("colour", colour);
        searchArgs.putBoolean("New", New);
        searchArgs.putBoolean("Used", Used);
        searchArgs.putBoolean("Selling", Selling);
        searchArgs.putBoolean("Wanted", Wanted);
        searchArgs.putString("previousFragment", previousFragment);
        searchArgs.putString("typeId", typeId);
        searchArgs.putString("plateCountry", plateCountry);
        searchArgs.putString("typeOfDelivery", typeOfDelivery);
        searchArgs.putString("typeGarageId", typeGarageId);
        searchArgs.putString("transportationCompanies", transportationCompanies);


    }

    public Bundle getSearch() {
        if (searchArgs.isEmpty()) {
        }
        return searchArgs;

    }

    public int getNumberOfChats() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                numberOfChats, 0);
        int value = sharedPreferences.getInt(numberOfChats, 0);
        return value;
    }

    public void setNumberOfChats(int status) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(numberOfChats,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(numberOfChats, status);
        editor.apply();
    }


    /**
     * this method is responsible for user logout and clearing cache
     */
    public void Logout() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        setFirstTime(false);
    }


}
