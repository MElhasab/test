package com.elhasab.fialhal.Utils;

public class Constants {
    public static final String MAP_VIEW_BUNDLE_KEY ="AIzaSyDtmlhdP_mh9JygKgor_cJc6kSgH1I-8eg" ;
    final static String PREFS_NAME = "settings";
    public static final String beforApiToken = "Bearer ";
    public final static String baseUrl = "https://cinderellaapp.com/api/";
    public static final String mobileToken = "mobile_token";
    public static final String CURRENT = "CURRENT";
    public static final String PREVIOUS = "PREVIOUS";
    public static final int PICK_IMAGE_REQUEST = 1;
    public static final String CLIENT = "client";
    public static final String SALON = "salon";
    public static final String most_service = "most_service";
    public static final String new_services = "new_services";
    public static final String most_rates = "most_rates";
    public static final String low_price = "low_price";
    public static final String high_price = "high_price";


//    'admin','new_order','salon_accept_order',
//    'client_accept_order','salon_finish',
//    'client_accept_finish','salon_canceled',
//    'client_accept_cancel'
}