package com.elhasab.fialhal.SplashAndIntro.Splash.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.elhasab.fialhal.SplashAndIntro.Intro.Intro1Activity;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import spencerstudios.com.bungeelib.Bungee;

public class SplashPresenter extends BasePresenter implements SplashViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public SplashPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void go() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (sharedPrefManager.getLoginStatus()) {
                    if (sharedPrefManager.getUserDate().getType().equals("client")) {
                        Intent intent = new Intent(context, HomeUserActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        context.startActivity(intent);
                        Bungee.split(context);
                    } else {
                        Intent intent = new Intent(context, HomeWorkerActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        context.startActivity(intent);
                        Bungee.split(context);
                    }
                } else {
                    Intent intent = new Intent(context, Intro1Activity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    context.startActivity(intent);
                    Bungee.split(context);
                }

            }
        }, 3000);

    }
}
