package com.elhasab.fialhal.SplashAndIntro.Intro;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.elhasab.fialhal.Authentication.Login.View.LoginActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;

public class Intro3Activity extends ParentClass {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_intro3);
        ButterKnife.bind(this);
        if (getLang(this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right);
        } else {
            ivBack.setImageResource(R.mipmap.back_left);
        }
    }

    @OnClick(R.id.ivBack)
    void back() {
        onBackPressed();
    }

    @OnClick(R.id.tvSkip)
    void skip() {
        Intent intent = new Intent(Intro3Activity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        Bungee.split(this);
    }

    @OnClick(R.id.ivNext)
    void next() {
        Intent intent = new Intent(Intro3Activity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        Bungee.split(this);
    }
}