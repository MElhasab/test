
package com.elhasab.fialhal.Models.ClientOrdersModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offer {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("worker_id")
    @Expose
    private int workerId;
    @SerializedName("worker_username")
    @Expose
    private String workerUsername;
    @SerializedName("worker_mobile")
    @Expose
    private String workerMobile;
    @SerializedName("worker_email")
    @Expose
    private String workerEmail;
    @SerializedName("worker_image")
    @Expose
    private String workerImage;
    @SerializedName("worker_rate")
    @Expose
    private int workerRate;
    @SerializedName("worker_address")
    @Expose
    private String workerAddress;
    @SerializedName("worker_lat")
    @Expose
    private String workerLat;
    @SerializedName("worker_lng")
    @Expose
    private String workerLng;
    @SerializedName("worker_job")
    @Expose
    private String workerJob;
    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("time")
    @Expose
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public String getWorkerUsername() {
        return workerUsername;
    }

    public void setWorkerUsername(String workerUsername) {
        this.workerUsername = workerUsername;
    }

    public String getWorkerMobile() {
        return workerMobile;
    }

    public void setWorkerMobile(String workerMobile) {
        this.workerMobile = workerMobile;
    }

    public String getWorkerEmail() {
        return workerEmail;
    }

    public void setWorkerEmail(String workerEmail) {
        this.workerEmail = workerEmail;
    }

    public String getWorkerImage() {
        return workerImage;
    }

    public void setWorkerImage(String workerImage) {
        this.workerImage = workerImage;
    }

    public int getWorkerRate() {
        return workerRate;
    }

    public void setWorkerRate(int workerRate) {
        this.workerRate = workerRate;
    }

    public String getWorkerAddress() {
        return workerAddress;
    }

    public void setWorkerAddress(String workerAddress) {
        this.workerAddress = workerAddress;
    }

    public String getWorkerLat() {
        return workerLat;
    }

    public void setWorkerLat(String workerLat) {
        this.workerLat = workerLat;
    }

    public String getWorkerLng() {
        return workerLng;
    }

    public void setWorkerLng(String workerLng) {
        this.workerLng = workerLng;
    }

    public String getWorkerJob() {
        return workerJob;
    }

    public void setWorkerJob(String workerJob) {
        this.workerJob = workerJob;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
