
package com.elhasab.fialhal.Models.CountriesModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountriesModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("value")
    @Expose
    private boolean value;
    @SerializedName("code")
    @Expose
    private int code;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

}
