
package com.elhasab.fialhal.Models.AllOrdersModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offer {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("worker_id")
    @Expose
    private int workerId;
    @SerializedName("worker_username")
    @Expose
    private String workerUsername;
    @SerializedName("worker_mobile")
    @Expose
    private String workerMobile;
    @SerializedName("worker_email")
    @Expose
    private String workerEmail;
    @SerializedName("worker_image")
    @Expose
    private String workerImage;
    @SerializedName("worker_rate")
    @Expose
    private int workerRate;
    @SerializedName("worker_address")
    @Expose
    private String workerAddress;
    @SerializedName("worker_lat")
    @Expose
    private String workerLat;
    @SerializedName("worker_lng")
    @Expose
    private String workerLng;
    @SerializedName("worker_job")
    @Expose
    private String workerJob;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("time")
    @Expose
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Offer withId(int id) {
        this.id = id;
        return this;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public Offer withWorkerId(int workerId) {
        this.workerId = workerId;
        return this;
    }

    public String getWorkerUsername() {
        return workerUsername;
    }

    public void setWorkerUsername(String workerUsername) {
        this.workerUsername = workerUsername;
    }

    public Offer withWorkerUsername(String workerUsername) {
        this.workerUsername = workerUsername;
        return this;
    }

    public String getWorkerMobile() {
        return workerMobile;
    }

    public void setWorkerMobile(String workerMobile) {
        this.workerMobile = workerMobile;
    }

    public Offer withWorkerMobile(String workerMobile) {
        this.workerMobile = workerMobile;
        return this;
    }

    public String getWorkerEmail() {
        return workerEmail;
    }

    public void setWorkerEmail(String workerEmail) {
        this.workerEmail = workerEmail;
    }

    public Offer withWorkerEmail(String workerEmail) {
        this.workerEmail = workerEmail;
        return this;
    }

    public String getWorkerImage() {
        return workerImage;
    }

    public void setWorkerImage(String workerImage) {
        this.workerImage = workerImage;
    }

    public Offer withWorkerImage(String workerImage) {
        this.workerImage = workerImage;
        return this;
    }

    public int getWorkerRate() {
        return workerRate;
    }

    public void setWorkerRate(int workerRate) {
        this.workerRate = workerRate;
    }

    public Offer withWorkerRate(int workerRate) {
        this.workerRate = workerRate;
        return this;
    }

    public String getWorkerAddress() {
        return workerAddress;
    }

    public void setWorkerAddress(String workerAddress) {
        this.workerAddress = workerAddress;
    }

    public Offer withWorkerAddress(String workerAddress) {
        this.workerAddress = workerAddress;
        return this;
    }

    public String getWorkerLat() {
        return workerLat;
    }

    public void setWorkerLat(String workerLat) {
        this.workerLat = workerLat;
    }

    public Offer withWorkerLat(String workerLat) {
        this.workerLat = workerLat;
        return this;
    }

    public String getWorkerLng() {
        return workerLng;
    }

    public void setWorkerLng(String workerLng) {
        this.workerLng = workerLng;
    }

    public Offer withWorkerLng(String workerLng) {
        this.workerLng = workerLng;
        return this;
    }

    public String getWorkerJob() {
        return workerJob;
    }

    public void setWorkerJob(String workerJob) {
        this.workerJob = workerJob;
    }

    public Offer withWorkerJob(String workerJob) {
        this.workerJob = workerJob;
        return this;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Offer withPrice(String price) {
        this.price = price;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Offer withComment(String comment) {
        this.comment = comment;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Offer withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Offer withTime(String time) {
        this.time = time;
        return this;
    }

}
