
package com.elhasab.fialhal.Models.CheckRoomModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("sender_name")
    @Expose
    private String senderName;
    @SerializedName("last_message")
    @Expose
    private String lastMessage;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("chats")
    @Expose
    private List<Object> chats = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Object> getChats() {
        return chats;
    }

    public void setChats(List<Object> chats) {
        this.chats = chats;
    }

}
