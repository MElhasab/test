
package com.elhasab.fialhal.Models.SettingModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("policy")
    @Expose
    private String policy;
    @SerializedName("single_commission")
    @Expose
    private String singleCommission;
    @SerializedName("multiple_commission")
    @Expose
    private String multipleCommission;
    @SerializedName("twitter")
    @Expose
    private String twitter;
    @SerializedName("facebook")
    @Expose
    private String facebook;
    @SerializedName("sanp")
    @Expose
    private String sanp;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("android_link")
    @Expose
    private String androidLink;
    @SerializedName("fee")
    @Expose
    private String fee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSingleCommission() {
        return singleCommission;
    }

    public void setSingleCommission(String singleCommission) {
        this.singleCommission = singleCommission;
    }

    public String getMultipleCommission() {
        return multipleCommission;
    }

    public void setMultipleCommission(String multipleCommission) {
        this.multipleCommission = multipleCommission;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getSanp() {
        return sanp;
    }

    public void setSanp(String sanp) {
        this.sanp = sanp;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAndroidLink() {
        return androidLink;
    }

    public void setAndroidLink(String androidLink) {
        this.androidLink = androidLink;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

}
