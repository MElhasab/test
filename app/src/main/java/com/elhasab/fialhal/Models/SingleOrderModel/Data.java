
package com.elhasab.fialhal.Models.SingleOrderModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("owner_id")
    @Expose
    private int ownerId;
    @SerializedName("owner_username")
    @Expose
    private String ownerUsername;
    @SerializedName("owner_mobile")
    @Expose
    private String ownerMobile;
    @SerializedName("owner_email")
    @Expose
    private String ownerEmail;
    @SerializedName("owner_image")
    @Expose
    private String ownerImage;
    @SerializedName("user_rate")
    @Expose
    private int userRate;
    @SerializedName("worker_id")
    @Expose
    private int workerId;
    @SerializedName("worker_username")
    @Expose
    private String workerUsername;
    @SerializedName("worker_mobile")
    @Expose
    private String workerMobile;
    @SerializedName("worker_email")
    @Expose
    private String workerEmail;
    @SerializedName("worker_image")
    @Expose
    private String workerImage;
    @SerializedName("worker_rate")
    @Expose
    private int workerRate;
    @SerializedName("city_id")
    @Expose
    private int cityId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("finish_time")
    @Expose
    private String finishTime;
    @SerializedName("price_offer")
    @Expose
    private String priceOffer;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("additional_fee")
    @Expose
    private int additionalFee;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("finish_code")
    @Expose
    private String finishCode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("offers")
    @Expose
    private List<Offer> offers = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Data withId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Data withTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Data withDescription(String description) {
        this.description = description;
        return this;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public Data withOwnerId(int ownerId) {
        this.ownerId = ownerId;
        return this;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public Data withOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
        return this;
    }

    public String getOwnerMobile() {
        return ownerMobile;
    }

    public void setOwnerMobile(String ownerMobile) {
        this.ownerMobile = ownerMobile;
    }

    public Data withOwnerMobile(String ownerMobile) {
        this.ownerMobile = ownerMobile;
        return this;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public Data withOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
        return this;
    }

    public String getOwnerImage() {
        return ownerImage;
    }

    public void setOwnerImage(String ownerImage) {
        this.ownerImage = ownerImage;
    }

    public Data withOwnerImage(String ownerImage) {
        this.ownerImage = ownerImage;
        return this;
    }

    public int getUserRate() {
        return userRate;
    }

    public void setUserRate(int userRate) {
        this.userRate = userRate;
    }

    public Data withUserRate(int userRate) {
        this.userRate = userRate;
        return this;
    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public Data withWorkerId(int workerId) {
        this.workerId = workerId;
        return this;
    }

    public String getWorkerUsername() {
        return workerUsername;
    }

    public void setWorkerUsername(String workerUsername) {
        this.workerUsername = workerUsername;
    }

    public Data withWorkerUsername(String workerUsername) {
        this.workerUsername = workerUsername;
        return this;
    }

    public String getWorkerMobile() {
        return workerMobile;
    }

    public void setWorkerMobile(String workerMobile) {
        this.workerMobile = workerMobile;
    }

    public Data withWorkerMobile(String workerMobile) {
        this.workerMobile = workerMobile;
        return this;
    }

    public String getWorkerEmail() {
        return workerEmail;
    }

    public void setWorkerEmail(String workerEmail) {
        this.workerEmail = workerEmail;
    }

    public Data withWorkerEmail(String workerEmail) {
        this.workerEmail = workerEmail;
        return this;
    }

    public String getWorkerImage() {
        return workerImage;
    }

    public void setWorkerImage(String workerImage) {
        this.workerImage = workerImage;
    }

    public Data withWorkerImage(String workerImage) {
        this.workerImage = workerImage;
        return this;
    }

    public int getWorkerRate() {
        return workerRate;
    }

    public void setWorkerRate(int workerRate) {
        this.workerRate = workerRate;
    }

    public Data withWorkerRate(int workerRate) {
        this.workerRate = workerRate;
        return this;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public Data withCityId(int cityId) {
        this.cityId = cityId;
        return this;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Data withCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public Data withLat(String lat) {
        this.lat = lat;
        return this;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Data withLng(String lng) {
        this.lng = lng;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Data withAddress(String address) {
        this.address = address;
        return this;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Data withDate(String date) {
        this.date = date;
        return this;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Data withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public Data withFinishTime(String finishTime) {
        this.finishTime = finishTime;
        return this;
    }

    public String getPriceOffer() {
        return priceOffer;
    }

    public void setPriceOffer(String priceOffer) {
        this.priceOffer = priceOffer;
    }

    public Data withPriceOffer(String priceOffer) {
        this.priceOffer = priceOffer;
        return this;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Data withTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public int getAdditionalFee() {
        return additionalFee;
    }

    public void setAdditionalFee(int additionalFee) {
        this.additionalFee = additionalFee;
    }

    public Data withAdditionalFee(int additionalFee) {
        this.additionalFee = additionalFee;
        return this;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Data withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public String getFinishCode() {
        return finishCode;
    }

    public void setFinishCode(String finishCode) {
        this.finishCode = finishCode;
    }

    public Data withFinishCode(String finishCode) {
        this.finishCode = finishCode;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data withStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Data withCode(String code) {
        this.code = code;
        return this;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Data withImages(List<Image> images) {
        this.images = images;
        return this;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Data withCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public Data withOffers(List<Offer> offers) {
        this.offers = offers;
        return this;
    }

}
