package com.elhasab.fialhal.Worker.RequestToWithdrawMoney.Presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.elhasab.fialhal.Models.GeneralResponse;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class RequestToWithdrawMoneyPresenter extends BasePresenter implements RequestToWithdrawMoneyViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;


    public RequestToWithdrawMoneyPresenter(Context context,SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }


    @Override
    public void requestTransaction(EditText editText) {
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.setError(context.getString(R.string.amountOfMoney));
            focusView = editText;
            cancel = true;
        }



        if (cancel) {

        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).requestTransfer(
                    Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(),
                    editText.getText().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<GeneralResponse> observer = new Observer<GeneralResponse>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(GeneralResponse userModel) {
                    stopLoading();
                    if (userModel != null) {
                        if (userModel.isValue()) {
                            successToast(context,userModel.getMsg());
                            Intent intent = new Intent(context,HomeWorkerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);

                        } else {
                            errorToast(context,userModel.getMsg());
                        }
                    } else {
                        errorToast(context,context.getString(R.string.someThingWentWrong));
                    }

                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context,e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }
    }
}
