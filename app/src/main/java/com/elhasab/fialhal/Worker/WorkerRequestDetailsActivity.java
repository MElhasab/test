package com.elhasab.fialhal.Worker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;

public class WorkerRequestDetailsActivity extends ParentClass {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_request_details);
    }
}