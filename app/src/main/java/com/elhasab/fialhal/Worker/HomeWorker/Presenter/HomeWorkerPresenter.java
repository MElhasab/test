package com.elhasab.fialhal.Worker.HomeWorker.Presenter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.AllOrdersModel.AllOrdersModel;
import com.elhasab.fialhal.Models.SettingModel.SettingModel;
import com.elhasab.fialhal.Models.WorkerHomeModel.WorkerHomeModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.Network.MainURL;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.UserOrdersAdapter;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.Adapter.RequestAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeWorkerPresenter extends BasePresenter implements HomeWorkerViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    int visibleItemCount;
    int totalItemCount;
    int pastVisiblesItems;
    String nextUrl;
    Boolean loading = false;

    String requests = MainURL.worker_home;

    public HomeWorkerPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getRequeast(ShimmerRecyclerView rvRequests, ProgressBar pbPagenation) {
        final UserOrdersAdapter adapter = new UserOrdersAdapter(context, rvRequests);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvRequests.setLayoutManager(layoutManager);

        rvRequests.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false;
                            pbPagenation.setVisibility(View.VISIBLE);
                            Observable observable = RetroWeb.getClient().create(MainServices.class).worker_home(nextUrl)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread());
                            Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(AllOrdersModel workerHomeModel) {
                                    pbPagenation.setVisibility(View.GONE);
                                    if (workerHomeModel != null) {
                                        if (workerHomeModel.isValue()) {
                                            adapter.addAll(workerHomeModel.getData());
                                            adapter.notifyDataSetChanged();
                                            nextUrl = String.valueOf(workerHomeModel.getLinks().getNext());
                                            loading = !nextUrl.equals("null");
                                        } else {
                                            errorToast(context, context.getString(R.string.someThingWentWrong));
                                            recyclerView.setVisibility(View.GONE);
                                        }
                                    } else {
                                        errorToast(context, context.getString(R.string.someThingWentWrong));
                                        recyclerView.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {
                                    handleApiException(context, e);
                                    pbPagenation.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.GONE);
                                }

                                @Override
                                public void onComplete() {

                                }
                            };
                            observable.subscribe(observer);
                        }
                    }
                }
            }
        });

        Observable observable = RetroWeb.getClient().create(MainServices.class).worker_home(requests)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(AllOrdersModel workerHomeModel) {
                if (workerHomeModel != null) {
                    if (workerHomeModel.isValue()) {
                        rvRequests.hideShimmerAdapter();
                        adapter.addAll(workerHomeModel.getData());
                        adapter.notifyDataSetChanged();
                        nextUrl = String.valueOf(workerHomeModel.getLinks().getNext());

                        if (workerHomeModel.getData().size() == 0) {
                            rvRequests.setVisibility(View.GONE);
                        } else {
                            rvRequests.setVisibility(View.VISIBLE);
                        }

                        loading = !nextUrl.equals("null");
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                        rvRequests.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    rvRequests.setVisibility(View.GONE);
                }

            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                errorToast(context, context.getString(R.string.someThingWentWrong));
                rvRequests.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        rvRequests.setAdapter(adapter);

    }

    @Override
    public void setSettings() {
        Observable observable = RetroWeb.getClient().create(MainServices.class).settings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SettingModel> observer = new Observer<SettingModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SettingModel settingModel) {
                if (settingModel != null) {
                    if (settingModel.isValue()) {
                        sharedPrefManager.setSettingsData(settingModel.getData());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }
}
