package com.elhasab.fialhal.Worker.RequestToWithdrawMoney.Presenter;

import android.widget.EditText;

public interface RequestToWithdrawMoneyViewPresenter {

    void requestTransaction(EditText editText);
}
