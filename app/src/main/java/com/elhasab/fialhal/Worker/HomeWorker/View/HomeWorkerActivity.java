package com.elhasab.fialhal.Worker.HomeWorker.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.MenuListFragment.View.MenuListFragment;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.Presenter.HomeWorkerPresenter;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import java.util.Locale;

public class HomeWorkerActivity extends AppCompatActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.rvMyRequests)
    ShimmerRecyclerView rvRequests;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;
    FlowingDrawer mDrawer;

    SharedPrefManager sharedPrefManager;
    HomeWorkerPresenter homeWorkerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ParentClass.getLang(this).equals("en")) {
            setContentView(R.layout.activity_home_worker_nav_left);
            ButterKnife.bind(this);
        }
        if (ParentClass.getLang(this).equals("ar")) {
            setContentView(R.layout.activity_home_worker_nav_right);
            ButterKnife.bind(this);
        }
        setupMenu();
        initUI();
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        }
    }

    public void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        homeWorkerPresenter = new HomeWorkerPresenter(this, sharedPrefManager);
        homeWorkerPresenter.getRequeast(rvRequests, pbPagination);
        homeWorkerPresenter.setSettings();
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {

            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {

            }
        });
    }

    public void setupMenu() {
        FragmentManager fm = getSupportFragmentManager();
        MenuListFragment mMenuFragment = (MenuListFragment) fm.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }
    }

    @OnClick(R.id.ivMenu)
    void openMenu() {
        mDrawer.openMenu();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocal();
    }

    private void setLocal() {
        Locale locale = new Locale(ParentClass.getLang(this));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        }
    }
}