package com.elhasab.fialhal.Worker.HomeWorker.Presenter;

import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface HomeWorkerViewPresenter {
    void getRequeast(ShimmerRecyclerView rvRequests, ProgressBar pbPagenation);

    void setSettings();
}
