package com.elhasab.fialhal.Worker.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.WorkerHomeModel.Datum;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    Context context;
    SharedPrefManager sharedPrefManager;
    ShimmerRecyclerView rvObject;
    ArrayList<Datum> homeList;
    LayoutInflater layoutInflater;

    public RequestAdapter(Context context, SharedPrefManager sharedPrefManager, ShimmerRecyclerView rvSubject) {
        this.context = context;
        homeList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rvObject = rvSubject;
        this.sharedPrefManager = sharedPrefManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_request, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(homeList.get(position).getTitle());
        holder.tvDate.setText(homeList.get(position).getDate());
        holder.tvLocation.setText(homeList.get(position).getAddress());
        if (!homeList.get(position).getImages().equals("")) {
            ParentClass.LoadImageWithPicasso(homeList.get(position).getImages().get(0).getImage(), context, holder.ivOrderImage);
        } else {
            holder.ivOrderImage.setImageResource(R.drawable.default_image);
        }
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Datum> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvOrderTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvMyLocation)
        TextView tvLocation;
        @BindView(R.id.ivOrderImage)
        ImageView ivOrderImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
