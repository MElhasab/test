package com.elhasab.fialhal.Worker.RequestToWithdrawMoney.View;

import android.os.Bundle;
import android.widget.EditText;

import com.elhasab.fialhal.Worker.RequestToWithdrawMoney.Presenter.RequestToWithdrawMoneyPresenter;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

public class RequestToWithdrawMoneyActivity extends ParentClass {


    @BindView(R.id.etAmountOfMoney)
    EditText etAmountOfMoney;


    SharedPrefManager sharedPrefManager;
    RequestToWithdrawMoneyPresenter requestToWithdrawMoneyPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_to_withdraw_money);
        ButterKnife.bind(this);
        initUi();
    }

    private void initUi() {
        sharedPrefManager = new SharedPrefManager(RequestToWithdrawMoneyActivity.this);
        requestToWithdrawMoneyPresenter = new RequestToWithdrawMoneyPresenter(RequestToWithdrawMoneyActivity.this,sharedPrefManager);
    }

    @OnClick(R.id.ivBack)
    void ivBack() {
        finish();
        Bungee.split(RequestToWithdrawMoneyActivity.this);
    }

    @OnClick(R.id.tvLogin)
    void tvLogin() {
        requestToWithdrawMoneyPresenter.requestTransaction(etAmountOfMoney);
    }
}