package com.elhasab.fialhal.Worker.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.CategoryModel.Datum;
import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class CheckBoxAdapter extends RecyclerView.Adapter<CheckBoxAdapter.ViewHolder> {

    Context context;
    ArrayList<Datum> homeList;
    LayoutInflater layoutInflater;
    ShimmerRecyclerView rvOrders;
    SharedPrefManager sharedPrefManager;
    String type;
    String categotiesId;
    TextView tvConfirm;
    ArrayList<String> items = new ArrayList<>();


    public CheckBoxAdapter(Context context, ShimmerRecyclerView rvSubjects, SharedPrefManager sharedPrefManager
            , TextView tvConfirm, String type) {
        this.context = context;
        homeList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rvOrders = rvSubjects;
        this.tvConfirm = tvConfirm;
        this.type = type;

    }

    @NonNull
    @Override
    public CheckBoxAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_checkbox, parent, false);
        return new CheckBoxAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckBoxAdapter.ViewHolder holder, int position) {
        holder.tvTitle.setText(homeList.get(position).getName());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    items.add(String.valueOf(homeList.get(position).getId()));
                } else if (!isChecked) {
                    items.remove(String.valueOf(homeList.get(position).getId()));
                }
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categotiesId = TextUtils.join(",", items);
                if (categotiesId.equals("")) {
                    ParentClass.makeErrorToast(context, context.getString(R.string.chooseAtLeastOneCategory));
                } else {
                    completeRegister(type, categotiesId);
                }
            }
        });

    }

    void completeRegister(String type, String categotiesId) {
        ParentClass.showFlipDialog();
        Observable observable = RetroWeb.getClient().create(MainServices.class).select_categories(Constants.beforApiToken
                + sharedPrefManager.getUserDate().getToken(), type, categotiesId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<UserModel> observer = new Observer<UserModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UserModel userModel) {
                ParentClass.dismissFlipDialog();
                if (userModel != null) {
                    if (userModel.isValue()) {
                        sharedPrefManager.setUserDate(userModel.getData());
                        sharedPrefManager.setLoginStatus(true);

                        Intent intent = new Intent(context, HomeWorkerActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        context.startActivity(intent);
                        Bungee.split(context);

                    } else {
                        ParentClass.makeErrorToast(context, context.getString(R.string.someThingWentWrong));
                    }

                } else {
                    ParentClass.makeErrorToast(context, context.getString(R.string.someThingWentWrong));
                }

            }

            @Override
            public void onError(Throwable e) {
                ParentClass.handleException(context, e);
                ParentClass.dismissFlipDialog();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            rvOrders.showShimmerAdapter();
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Datum> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.checkBox)
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
