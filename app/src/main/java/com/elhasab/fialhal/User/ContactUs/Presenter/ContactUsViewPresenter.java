package com.elhasab.fialhal.User.ContactUs.Presenter;

import android.widget.EditText;

public interface ContactUsViewPresenter {
    void contactUs(EditText etName,EditText etMobile,EditText etEmail,EditText etDescription);
}
