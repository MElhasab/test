package com.elhasab.fialhal.User.Profile.Presenter;

import android.widget.EditText;
import android.widget.Spinner;

import okhttp3.MultipartBody;

public interface ProfileViewPresenter {
    void getCountries(Spinner spCountries, String cityId);

    void updateProfile(EditText username, EditText email, EditText mobile, MultipartBody.Part multipartBodyImage, String cityId
            , Boolean imageSelected, EditText experience);
}
