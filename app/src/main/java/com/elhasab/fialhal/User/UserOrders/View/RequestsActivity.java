package com.elhasab.fialhal.User.UserOrders.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.UserOrders.Presenter.UserOrdersActivityPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class RequestsActivity extends AppCompatActivity {

    @BindView(R.id.viewCurrent)
    View viewCurrent;
    @BindView(R.id.viewInprogress)
    View viewInprogress;
    @BindView(R.id.viewComplete)
    View viewComplete;
    @BindView(R.id.viewCanceled)
    View viewCanceled;
    @BindView(R.id.rvMyRequests)
    ShimmerRecyclerView rvUserOrders;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;
    @BindView(R.id.tvCurrent)
    TextView tvWaiting;
    SharedPrefManager sharedPrefManager;
    String type = "";
    UserOrdersActivityPresenter userOrdersActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests);
        ButterKnife.bind(this);
        initUi();
    }

    public void initUi() {
        sharedPrefManager = new SharedPrefManager(this);
        userOrdersActivityPresenter = new UserOrdersActivityPresenter(this, sharedPrefManager);
    }

    @OnClick(R.id.rlCurrent)
    void rlCurrent() {
        if (getIntent().getStringExtra("type").equals("worker")) {
            if (!type.equals("waiting")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "waiting";
                userOrdersActivityPresenter.getWorkerOrders(rvUserOrders, pbPagination, type);
            } else if (!type.equals("new")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "new";
                userOrdersActivityPresenter.getOrders(rvUserOrders, pbPagination, type);
            }
        }
    }

    @OnClick(R.id.rlInprogress)
    void rlInprogress() {
        if (getIntent().getStringExtra("type").equals("worker")) {
            if (!type.equals("in_progress")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "in_progress";
                userOrdersActivityPresenter.getWorkerOrders(rvUserOrders, pbPagination, type);
            } else if (!type.equals("in_progress")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "in_progress";
                userOrdersActivityPresenter.getOrders(rvUserOrders, pbPagination, type);
            }
        }
    }

    @OnClick(R.id.rlComplete)
    void rlComplete() {
        if (getIntent().getStringExtra("type").equals("worker")) {
            if (!type.equals("finish")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "finish";
                userOrdersActivityPresenter.getWorkerOrders(rvUserOrders, pbPagination, type);
            } else if (!type.equals("finish")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "finish";
                userOrdersActivityPresenter.getOrders(rvUserOrders, pbPagination, type);
            }
        }
    }

    @OnClick(R.id.rlCanceled)
    void rlCanceled() {
        if (getIntent().getStringExtra("type").equals("worker")) {
            if (!type.equals("canceled")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "canceled";
                userOrdersActivityPresenter.getWorkerOrders(rvUserOrders, pbPagination, type);
            } else if (!type.equals("canceled")) {
                viewCurrent.setVisibility(View.VISIBLE);
                viewInprogress.setVisibility(View.GONE);
                viewComplete.setVisibility(View.GONE);
                viewCanceled.setVisibility(View.GONE);
                type = "canceled";
                userOrdersActivityPresenter.getOrders(rvUserOrders, pbPagination, type);
            }
        }
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
        Bungee.split(this);
    }
}