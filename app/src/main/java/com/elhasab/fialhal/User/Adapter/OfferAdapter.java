package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.SingleOrderModel.Offer;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.OrderDetails.View.OrderDetailsActivity;
import com.elhasab.fialhal.Utils.ParentClass;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.ViewHolder> {
    Context context;
    ArrayList<Offer> homeList;
    LayoutInflater layoutInflater;
    ShimmerRecyclerView rvOffers;

    public OfferAdapter(Context context, ShimmerRecyclerView rvOffers) {
        homeList = new ArrayList<>();
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.rvOffers = rvOffers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_offer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (ParentClass.sharedPrefManager.getUserDate().getType().equals("worker")) {
            holder.tvWatchOffer.setVisibility(View.GONE);
        }
        ParentClass.LoadImageWithPicasso(homeList.get(position).getWorkerImage(), context, holder.ivOwnerImage);
        holder.tvDate.setText(homeList.get(position).getTime());
        holder.tvOfferBody.setText(homeList.get(position).getComment());
        holder.tvDate.setText(homeList.get(position).getTime());
        holder.tvOfferOwnerRating.setText(homeList.get(position).getWorkerRate());
        holder.tvOwnerName.setText(homeList.get(position).getWorkerUsername());
        holder.tvOfferPrice.setText(homeList.get(position).getPrice() + " " + context.getString(R.string.sar));
        holder.tvOwnerLocation.setText(homeList.get(position).getWorkerAddress());
        holder.tvWatchOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("type", homeList.get(position).getStatus());
                intent.putExtra("id", String.valueOf(homeList.get(position).getOrder_id()));
                intent.putExtra("position", String.valueOf(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            rvOffers.showShimmerAdapter();
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Offer> data) {
        homeList.clear();
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivOfferOwnerImage)
        ImageView ivOwnerImage;
        @BindView(R.id.tvOfferOwnerName)
        TextView tvOwnerName;
        @BindView(R.id.tvOfferOwnerLocation)
        TextView tvOwnerLocation;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvOfferBody)
        TextView tvOfferBody;
        @BindView(R.id.tvOfferPrice)
        TextView tvOfferPrice;
        @BindView(R.id.tvWatchOffer)
        TextView tvWatchOffer;
        @BindView(R.id.tvOfferOwnerRating)
        TextView tvOfferOwnerRating;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
