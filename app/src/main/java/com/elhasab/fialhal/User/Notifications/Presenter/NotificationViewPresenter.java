package com.elhasab.fialhal.User.Notifications.Presenter;

import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface NotificationViewPresenter {
    void getNotification(ShimmerRecyclerView recyclerView, ProgressBar pbPagination);
}
