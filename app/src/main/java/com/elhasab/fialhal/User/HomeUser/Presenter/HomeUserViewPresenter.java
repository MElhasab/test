package com.elhasab.fialhal.User.HomeUser.Presenter;

import android.widget.ProgressBar;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import java.util.ArrayList;

public interface HomeUserViewPresenter {
    void setSetting();

    void getCategory(ShimmerRecyclerView rvCategory, TextView tvConfirm, String categoriesId, ArrayList<String> items);

}
