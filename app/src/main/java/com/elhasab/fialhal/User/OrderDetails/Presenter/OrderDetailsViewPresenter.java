package com.elhasab.fialhal.User.OrderDetails.Presenter;

import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;

public interface OrderDetailsViewPresenter {
    void getOrderDetails(TextView tvOrderNumber, TextView tvUsername, TextView tvStatus
            , TextView tvWorkerName, TextView tvReservationNumper, TextView tvWorkerAddress, TextView tvDateAndTime
            , TextView tvPrice, ImageView ivProfileImage, GoogleMap gmap, String id
            , String position, TextView tvOfferId);

    void responseOffer(TextView tvOfferId, String type);
}
