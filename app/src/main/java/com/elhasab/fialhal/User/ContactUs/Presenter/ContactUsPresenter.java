package com.elhasab.fialhal.User.ContactUs.Presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.elhasab.fialhal.Models.GeneralResponse;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class ContactUsPresenter extends BasePresenter implements ContactUsViewPresenter {

    Context context;
    SharedPrefManager sharedPrefManager;

    public ContactUsPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void contactUs(EditText etName, EditText etMobile, EditText etEmail, EditText etDescription) {
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            etName.setError(context.getString(R.string.username));
            focusView = etName;
            cancel = true;
        }

        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError(context.getString(R.string.email));
            focusView = etEmail;
            cancel = true;
        }
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            etMobile.setError(context.getString(R.string.mobileNum));
            focusView = etMobile;
            cancel = true;
        }
        if (TextUtils.isEmpty(etDescription.getText().toString())) {
            etDescription.setError(context.getString(R.string.description));
            focusView = etDescription;
            cancel = true;
        }

        if (cancel) {
        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).contact_us(etName.getText().toString(),
                    etMobile.getText().toString(), etEmail.getText().toString(), etDescription.getText().toString())
                    .observeOn(Schedulers.io())
                    .subscribeOn(AndroidSchedulers.mainThread());
            Observer<GeneralResponse> observer = new Observer<GeneralResponse>() {
                @Override
                public void onSubscribe(@NonNull Disposable d) {

                }

                @Override
                public void onNext(@NonNull GeneralResponse generalResponse) {
                    stopLoading();
                    if (generalResponse != null) {
                        if (generalResponse.isValue()) {
                            successToast(context, generalResponse.getMsg());
                            if (sharedPrefManager.getUserDate().getType().equals("client")) {
                                Intent intent = new Intent(context, HomeUserActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                            } else {
                                Intent intent = new Intent(context, HomeWorkerActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                            }
                            Bungee.split(context);
                        } else {
                            errorToast(context, generalResponse.getMsg());
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }
    }
}
