package com.elhasab.fialhal.User.RequestDetails.View;

import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.daimajia.slider.library.SliderLayout;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Chat.View.ChatActivity;
import com.elhasab.fialhal.User.FollowingOrder.View.FollowingOrderActivity;
import com.elhasab.fialhal.User.RequestDetails.Presenter.RequestDetailsPresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class RequestDetailsActivity extends ParentClass {

    @BindView(R.id.sliderLayout)
    SliderLayout sliderLayout;
    @BindView(R.id.rlEdit)
    RelativeLayout relativeUpdate;
    @BindView(R.id.rlDelete)
    RelativeLayout relativeDelete;
    @BindView(R.id.tvMyLocation)
    TextView tvLocation;
    @BindView(R.id.tvOrderTitle)
    TextView tvTitle;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvOrderBody)
    TextView tvDescription;
    @BindView(R.id.relativePayment)
    RelativeLayout relativeInfoForEveryStatusButInProgress;
    @BindView(R.id.tvPayment)
    TextView tvPaymentWay;
    @BindView(R.id.tvTax)
    TextView tvTax;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvCancelOrder)
    TextView tvCancelOrder;
    @BindView(R.id.rvOffers)
    ShimmerRecyclerView rvOffers;
    @BindView(R.id.relativeNoImage)
    RelativeLayout relativeNoImage;
    @BindView(R.id.tvOffers)
    TextView tvOffersTitle;
    RequestDetailsPresenter requestDetailsPresenter;
    SharedPrefManager sharedPrefManager;


    @BindView(R.id.relativeContact)
    RelativeLayout relativeContact;
    @BindView(R.id.tvCallNow)
    TextView tvCallNow;
    @BindView(R.id.tvContactNow)
    TextView tvContactNow;
    @BindView(R.id.tvTrackOrder)
    TextView tvTrackOrder;
    @BindView(R.id.tvMobile)
    TextView tvMobile;
    @BindView(R.id.tvWorkerId)
    TextView tvWorkerId;
    @BindView(R.id.tvWorkerName)
    TextView tvWorkerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        ButterKnife.bind(this);
        initUi();
    }

    public void initUi() {
        sharedPrefManager = new SharedPrefManager(this);
        requestDetailsPresenter = new RequestDetailsPresenter(this, sharedPrefManager);

        requestDetailsPresenter.getSingelOrder(sliderLayout, relativeUpdate, relativeDelete, tvLocation, tvTitle, tvDate
                , tvDescription, relativeInfoForEveryStatusButInProgress, tvPaymentWay, tvTax, tvTotal, tvCancelOrder, rvOffers
                , getIntent().getStringExtra("id"), getIntent().getStringExtra("type"), relativeNoImage, tvOffersTitle
                , relativeContact, tvCallNow, tvContactNow, tvTrackOrder, tvMobile, tvWorkerId, tvWorkerName);

    }

    @OnClick(R.id.ivBack)
    void goBack() {
        finish();
    }

    @OnClick(R.id.tvCallNow)
    void call() {
        try {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tvMobile.getText().toString()));

            if (ActivityCompat.checkSelfPermission(RequestDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        } catch (Exception e) {
        }
    }

    @OnClick(R.id.tvContactNow)
    void contact() {
        Intent intent = new Intent(RequestDetailsActivity.this, ChatActivity.class);
        intent.putExtra("sellerName", tvWorkerName.getText().toString());
        intent.putExtra("sellerId", tvWorkerId.getText().toString());
        intent.putExtra("type", "details");
        startActivity(intent);
        Bungee.split(RequestDetailsActivity.this);
    }

    @OnClick(R.id.tvTrackOrder)
    void trackOrder() {
        Intent intent = new Intent(RequestDetailsActivity.this, FollowingOrderActivity.class);
        intent.putExtra("sellerName", tvWorkerName.getText().toString());
        intent.putExtra("sellerId", tvWorkerId.getText().toString());
        intent.putExtra("type", "details");
        startActivity(intent);
        Bungee.split(RequestDetailsActivity.this);
    }

    @OnClick(R.id.tvCancelOrder)
    void cancelOrder() {
        requestDetailsPresenter.cancelOrder(getIntent().getStringExtra("id"));
    }

    @OnClick(R.id.rlDelete)
    void deleteOrder() {
        requestDetailsPresenter.deleteOrder(getIntent().getStringExtra("id"));
    }
}