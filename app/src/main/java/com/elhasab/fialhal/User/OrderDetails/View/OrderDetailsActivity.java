package com.elhasab.fialhal.User.OrderDetails.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.OrderDetails.Presenter.OrderDetailsPresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class OrderDetailsActivity extends ParentClass implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener {

    @BindView(R.id.tvOrderNumber)
    TextView tvOrderNumber;
    @BindView(R.id.ivProfileImage)
    ImageView ivProfileImage;
    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.tvClientWork)
    TextView tvStatus;
    @BindView(R.id.tvReservationNumber)
    TextView tvReservationNumber;
    @BindView(R.id.tvWorkerName)
    TextView tvWorkerName;
    @BindView(R.id.tvWorkerAddress)
    TextView tvWorkerAddress;
    @BindView(R.id.tvDateAndTime)
    TextView tvDateAndTime;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.mapView)
    MapView map;
    @BindView(R.id.tvAccept)
    TextView tvAccept;
    @BindView(R.id.tvReject)
    TextView tvReject;

    private static final int REQUEST_LOCATION_CODE = 99;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentLocationMarker;
    double latitude, longitude;
    MarkerOptions markerOptions;
    LatLng start_lat_lng;
    LatLng final_location;
    private GoogleMap gmap;
    Marker marker;
    String lat = "";
    String lng = "";
    SharedPrefManager sharedPrefManager;
    OrderDetailsPresenter orderDetailsPresenter;
    @BindView(R.id.tvOfferId)
    TextView tvOfferId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);
        Bundle mapViewBundle = null;

        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(Constants.MAP_VIEW_BUNDLE_KEY);
        }
        map.onCreate(mapViewBundle);
        map.getMapAsync(this);

        initUi();
    }

    private void initUi() {

        sharedPrefManager = new SharedPrefManager(OrderDetailsActivity.this);
        orderDetailsPresenter = new OrderDetailsPresenter(OrderDetailsActivity.this, sharedPrefManager);

    }

    @OnClick(R.id.ivBack)
    void ivBack() {
        finish();
    }

    @OnClick(R.id.tvAccept)
    void tvAcceptOrder() {
        orderDetailsPresenter.responseOffer(tvOfferId,"approve");

    }

    @OnClick(R.id.tvReject)
    void tvRejectOrder() {
        orderDetailsPresenter.responseOffer(tvOfferId,"decline");
    }


    @Override
    public void onLocationChanged(@NonNull Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(locationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this::onLocationChanged);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gmap = googleMap;
        gmap.setOnMarkerClickListener(this);
        if (ContextCompat.checkSelfPermission(OrderDetailsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleAPiClient();
            gmap.setMyLocationEnabled(true);
        }
        orderDetailsPresenter.getOrderDetails(tvOrderNumber, tvUsername, tvStatus, tvReservationNumber,
                tvWorkerName, tvWorkerAddress, tvReservationNumber, tvPrice, ivProfileImage,
                gmap, getIntent().getStringExtra("id"), getIntent().getStringExtra("position"), tvOfferId);
    }

    protected synchronized void buildGoogleAPiClient() {
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        client.connect();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(Constants.MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(Constants.MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        map.onSaveInstanceState(mapViewBundle);

    }


    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map.onLowMemory();
    }
}