package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.TransfaresModel.Datum;
import com.elhasab.fialhal.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TransferAdapter extends RecyclerView.Adapter<TransferAdapter.ViewHolder> {
    ArrayList<Datum> homeList;
    LayoutInflater layoutInflater;
    Context context;
    ShimmerRecyclerView rvObject;

    public TransferAdapter(Context context, ShimmerRecyclerView rvObject) {
        homeList = new ArrayList<>();
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.rvObject = rvObject;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_transactions, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvPrice.setText(homeList.get(position).getAmount());
        if (homeList.get(position).getType().equals("request")) {
            holder.tvType.setText(context.getString(R.string.balanceRequest));
        } else {
            holder.tvType.setText(context.getString(R.string.addBalance));
        }
        holder.tvTransactionDate.setText(homeList.get(position).getDate());
        holder.tvTransactionNumber.setText(homeList.get(position).getCode());

    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            rvObject.showShimmerAdapter();
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Datum> data) {
//        homeList.clear();
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvType)
        TextView tvType;
        @BindView(R.id.tvPrice)
        TextView tvPrice;
        @BindView(R.id.tvTransactionDate)
        TextView tvTransactionDate;
        @BindView(R.id.tvTransactionNumber)
        TextView tvTransactionNumber;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
