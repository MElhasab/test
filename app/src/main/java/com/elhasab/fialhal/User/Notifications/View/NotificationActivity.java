package com.elhasab.fialhal.User.Notifications.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Notifications.Presenter.NotificationPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import static com.elhasab.fialhal.Utils.ParentClass.getLang;

public class NotificationActivity extends AppCompatActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rvNotification)
    ShimmerRecyclerView rvNotifications;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;

    SharedPrefManager sharedPrefManager;
    NotificationPresenter notificationPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        if (getLang(NotificationActivity.this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right);
        } else {
            ivBack.setImageResource(R.mipmap.back_left);
        }
        sharedPrefManager = new SharedPrefManager(this);
        notificationPresenter = new NotificationPresenter(this, sharedPrefManager);
        notificationPresenter.getNotification(rvNotifications, pbPagination);
    }
}