package com.elhasab.fialhal.User.Wallet.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Wallet.Presenter.WalletPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.RequestToWithdrawMoney.View.RequestToWithdrawMoneyActivity;

public class WalletActivity extends AppCompatActivity {

    @BindView(R.id.tvNumOfPoints)
    TextView tvPoints;
    @BindView(R.id.tvRequestBalance)
    TextView tvRequestBalance;
    @BindView(R.id.rvTransactions)
    ShimmerRecyclerView rvTransaction;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;

    SharedPrefManager sharedPrefManager;
    WalletPresenter walletPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        walletPresenter = new WalletPresenter(this, sharedPrefManager);
        if (getIntent().getStringExtra("type").equals("worker")) {
            tvRequestBalance.setVisibility(View.VISIBLE);
        }
        if (sharedPrefManager.getUserDate().getWallet().equals("")) {
            tvPoints.setText("0" + " " + getString(R.string.point));
        } else {
            tvPoints.setText(sharedPrefManager.getUserDate().getWallet() + " " + getString(R.string.point));
        }
        walletPresenter.transfer(rvTransaction, pbPagination);
    }

    @OnClick(R.id.ivBack)
    void goBack() {
        finish();
        Bungee.split(this);
    }

    @OnClick(R.id.tvRequestBalance)
    void balance() {
        Intent intent = new Intent(this, RequestToWithdrawMoneyActivity.class);
        startActivity(intent);
        Bungee.split(this);
    }
}