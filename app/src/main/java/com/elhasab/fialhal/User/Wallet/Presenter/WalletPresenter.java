package com.elhasab.fialhal.User.Wallet.Presenter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.NotificationsModels.NotificationsModel;
import com.elhasab.fialhal.Models.TransfaresModel.TransfersModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.Network.MainURL;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.NotificationAdapter;
import com.elhasab.fialhal.User.Adapter.TransferAdapter;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WalletPresenter extends BasePresenter implements WalletViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    int visibleItemCount;
    int totalItemCount;
    int pastVisiblesItems;
    String nextUrl;
    Boolean loading = false;

    String transfer = MainURL.client_and_worker_transfers;

    public WalletPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void transfer(ShimmerRecyclerView recyclerView, ProgressBar pbPagination) {
        TransferAdapter adapter = new TransferAdapter(context, recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pbPagination.setVisibility(View.VISIBLE);
                            Observable observable = RetroWeb.getClient().create(MainServices.class).client_and_worker_transfers(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), nextUrl)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread());
                            Observer<TransfersModel> observer = new Observer<TransfersModel>() {
                                @Override
                                public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                                }

                                @Override
                                public void onNext(@io.reactivex.annotations.NonNull TransfersModel notificationsModel) {
                                    pbPagination.setVisibility(View.GONE);

                                    if (notificationsModel != null) {
                                        if (notificationsModel.isValue()) {

                                            adapter.addAll(notificationsModel.getData());
                                            adapter.notifyDataSetChanged();
                                            nextUrl = String.valueOf(notificationsModel.getLinks().getNext());
                                            try {
                                                loading = !nextUrl.equals("null");

                                            } catch (Exception e) {

                                            }
                                        } else {
                                            errorToast(context, context.getString(R.string.someThingWentWrong));
                                            recyclerView.setVisibility(View.GONE);
                                        }
                                    } else {
                                        errorToast(context, context.getString(R.string.someThingWentWrong));
                                        recyclerView.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                                    handleApiException(context, e);
                                    pbPagination.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.GONE);
                                }

                                @Override
                                public void onComplete() {

                                }
                            };
                            observable.subscribe(observer);
                        }
                    }
                }
            }
        });
        Observable observable = RetroWeb.getClient().create(MainServices.class).notifications(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), transfer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<TransfersModel> observer = new Observer<TransfersModel>() {
            @Override
            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.annotations.NonNull TransfersModel notificationsModel) {
                if (notificationsModel != null) {
                    if (notificationsModel.isValue()) {
                        recyclerView.hideShimmerAdapter();
                        if (notificationsModel.getData().size() != 0) {
                            adapter.addAll(notificationsModel.getData());
                            adapter.notifyDataSetChanged();
                        }
                        nextUrl = String.valueOf(notificationsModel.getLinks().getNext());
                        if (notificationsModel.getData().size() == 0) {
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                        try {
                            loading = !nextUrl.equals("null");

                        } catch (Exception e) {

                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                        recyclerView.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                handleApiException(context, e);
                recyclerView.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        recyclerView.setAdapter(adapter);
    }
}
