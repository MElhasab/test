package com.elhasab.fialhal.User.RequestDetails.Presenter;

import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.daimajia.slider.library.SliderLayout;

public interface RequestDetailsViewPresenter {
    void getSingelOrder( SliderLayout sliderLayout, RelativeLayout relativeUpdate,
                         RelativeLayout relativeDelete, TextView tvLocation,
                         TextView tvTitle, TextView tvDate, TextView tvDescription,
                         RelativeLayout relativeInfoForEveryStatusButInProgress,
                         TextView tvPaymentWay, TextView tvTax, TextView tvTotal,
                         TextView tvCancelOrder, ShimmerRecyclerView recyclerView,
                         String id, String type,
                         RelativeLayout relativeNoImage, TextView tvOffersTitle,
                         RelativeLayout relativeContact,TextView tvCallNow,
                         TextView tvContactNow,
                         TextView tvTrackOrder,TextView tvMobile,TextView tvWorkerId,TextView tvWorkerName);
    void cancelOrder(String id);

    void deleteOrder(String id);
}
