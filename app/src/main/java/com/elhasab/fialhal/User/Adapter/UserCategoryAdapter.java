package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.MainCategoryAndItsSub.Datum;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserCategoryAdapter extends RecyclerView.Adapter<UserCategoryAdapter.ViewHolder> {

    Context context;
    ShimmerRecyclerView rvObject;
    ArrayList<Datum> homeList;
    LayoutInflater layoutInflater;
    TextView tvConfirm;
    String categotiesId;
    ArrayList<String> items;

    public UserCategoryAdapter(Context context, ShimmerRecyclerView rvSubject, String categotiesId, ArrayList<String> items, TextView tvConfirm) {
        this.context = context;
        homeList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rvObject = rvSubject;
        this.tvConfirm = tvConfirm;
        this.categotiesId = categotiesId;
        this.items = items;

    }

    @NonNull
    @Override
    public UserCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_user_category, parent, false);
        return new UserCategoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserCategoryAdapter.ViewHolder holder, int position) {
        holder.tvTitle.setText(homeList.get(position).getName());
        UserSubCategoryAdapter adapter = new UserSubCategoryAdapter(context, categotiesId, items, tvConfirm);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        holder.rvSubCategories.setLayoutManager(layoutManager);
        adapter.addAll(homeList.get(position).getSubCategories());
        adapter.notifyDataSetChanged();
        holder.rvSubCategories.setAdapter(adapter);

        if (ParentClass.getLang(context).equals("ar")) {
            holder.ivArrow.setImageResource(R.mipmap.back_left);
        } else {
            holder.ivArrow.setImageResource(R.mipmap.back_right);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (homeList.get(position).getOpen().equals("1")) {
                    homeList.get(position).setOpen("0");
                    holder.view.setVisibility(View.GONE);
                    holder.rvSubCategories.setVisibility(View.GONE);
                    if (ParentClass.getLang(context).equals("ar")) {
                        holder.ivArrow.setImageResource(R.mipmap.back_left);
                    } else {
                        holder.ivArrow.setImageResource(R.mipmap.back_right);
                    }
                } else if (homeList.get(position).getOpen().equals("0")) {
                    homeList.get(position).setOpen("1");
                    holder.view.setVisibility(View.VISIBLE);
                    holder.rvSubCategories.setVisibility(View.VISIBLE);
                    holder.ivArrow.setImageResource(R.mipmap.down_arrow);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Datum> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.view)
        View view;
        @BindView(R.id.rvSubCategories)
        RecyclerView rvSubCategories;
        @BindView(R.id.ivArrow)
        ImageView ivArrow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}

