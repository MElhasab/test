package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.ChatHistoryModel.Data;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.ParentClass;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import spencerstudios.com.bungeelib.Bungee;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    ArrayList<Data> homeList;
    LayoutInflater layoutInflater;
    Context context;
    ShimmerRecyclerView rvObject;

    public ChatAdapter(Context context, ShimmerRecyclerView rvObject) {
        homeList = new ArrayList<>();
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.rvObject = rvObject;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setText(homeList.get(position).getSenderName());
        holder.tvBody.setText(homeList.get(position).getLastMessage());
        holder.tvDate.setText(homeList.get(position).getTime());
        ParentClass.LoadImageWithPicasso(homeList.get(position).getImage(), context, holder.ivUserImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HomeUserActivity.class);
                intent.putExtra("id", String.valueOf(homeList.get(position).getId()));
                context.startActivity(intent);
                Bungee.split(context);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Data> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivUserImage)
        ImageView ivUserImage;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvBody)
        TextView tvBody;
        @BindView(R.id.tvDateAndTime)
        TextView tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
