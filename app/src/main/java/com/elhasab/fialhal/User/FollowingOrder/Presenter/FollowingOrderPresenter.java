package com.elhasab.fialhal.User.FollowingOrder.Presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.VerificationCode.View.VerificationCodeActivity;
import com.elhasab.fialhal.Models.GeneralResponse;
import com.elhasab.fialhal.Models.SingleOrderModel.SingleOrderModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.w3c.dom.Text;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class FollowingOrderPresenter extends BasePresenter implements FollowingOrderViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public FollowingOrderPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void followingOrder(View viewSecondPhase, View viewThirdPhase, ImageView ivThirdPhase, ImageView ivForthPhase,
                               TextView tvBasedOnStatus, TextView tvStatus, String id) {
        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).single_orders(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SingleOrderModel singleOrderModel) {
                stopLoading();
                if (singleOrderModel != null) {
                    if (singleOrderModel.isValue()) {
                        tvStatus.setText(singleOrderModel.getData().getStatus());
                        if (sharedPrefManager.getUserDate().getType().equals("worker")) {
                            if (singleOrderModel.getData().getStatus().equals("in_progress")) {
                                tvBasedOnStatus.setVisibility(View.VISIBLE);
                                tvBasedOnStatus.setText(context.getString(R.string.startTheOrder));
                                viewSecondPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                viewThirdPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                ivThirdPhase.setImageResource(R.drawable.clean);
                                ivForthPhase.setImageResource(R.drawable.clean);
                            }
                            if (singleOrderModel.getData().getStatus().equals("worker_in_way")) {
                                if (singleOrderModel.getData().getFinishCode().equals("")) {
                                    tvBasedOnStatus.setVisibility(View.GONE);
                                    viewSecondPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                    viewThirdPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                    ivThirdPhase.setImageResource(R.drawable.radio);
                                    ivForthPhase.setImageResource(R.drawable.clean);
                                } else {
                                    tvBasedOnStatus.setVisibility(View.VISIBLE);
                                    tvBasedOnStatus.setText(context.getString(R.string.orderIsFinished));
                                    viewSecondPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                    viewThirdPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                    ivThirdPhase.setImageResource(R.drawable.radio);
                                    ivForthPhase.setImageResource(R.drawable.clean);
                                }
                            }
                            if (singleOrderModel.getData().getStatus().equals("worker_finish")) {
                                tvBasedOnStatus.setVisibility(View.VISIBLE);
                                tvBasedOnStatus.setText(context.getString(R.string.enterCode));
                                viewSecondPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                viewThirdPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                ivThirdPhase.setImageResource(R.drawable.radio);
                                ivForthPhase.setImageResource(R.drawable.radio);
                            }
                        } else {
                            if (singleOrderModel.getData().getStatus().equals("in_progress")) {
                                tvBasedOnStatus.setVisibility(View.GONE);
                                viewSecondPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                viewThirdPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                ivThirdPhase.setImageResource(R.drawable.clean);
                                ivForthPhase.setImageResource(R.drawable.clean);
                            }
                            if (singleOrderModel.getData().getStatus().equals("worker_in_way")) {
                                if (singleOrderModel.getData().getFinishCode().equals("")) {
                                    tvBasedOnStatus.setVisibility(View.VISIBLE);
                                    tvBasedOnStatus.setText(context.getString(R.string.orderIsFinished));
                                    viewSecondPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                    viewThirdPhase.setBackgroundColor(Color.parseColor("#788c9c"));
                                    ivThirdPhase.setImageResource(R.drawable.radio);
                                    ivForthPhase.setImageResource(R.drawable.clean);
                                } else {
                                    tvBasedOnStatus.setVisibility(View.GONE);
                                    viewSecondPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                    viewThirdPhase.setBackgroundColor(Color.parseColor("#E2A000"));
                                    ivThirdPhase.setImageResource(R.drawable.radio);
                                    ivForthPhase.setImageResource(R.drawable.radio);
                                }
                            }
                        }
                    } else {
                        errorToast(context, singleOrderModel.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                handleApiException(context, e);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    @Override
    public void rate(String id, String rate, EditText etRate) {
        boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etRate.getText().toString())) {
            etRate.setError(context.getString(R.string.addComment));
            focusView = etRate;
            cancel = true;
        }


        if (cancel) {

        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).
                    rate(Constants.beforApiToken +
                                    sharedPrefManager.getUserDate().getToken(), id, rate,
                            etRate.getText().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<GeneralResponse> observer = new Observer<GeneralResponse>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(GeneralResponse userModel) {
                    stopLoading();
                    if (userModel != null) {
                        if (userModel.isValue()) {
                            successToast(context, userModel.getMsg());
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);
                        } else {
                            errorToast(context, userModel.getMsg());
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }

                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }
    }

    @Override
    public void changeOrderStatus(String id, String status, BottomSheetDialog popAddRate) {

        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).change_order_status(
                Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(),
                id, status)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SingleOrderModel homeModel) {
                stopLoading();
                if (homeModel != null) {
                    if (homeModel.isValue()) {
                        if (sharedPrefManager.getUserDate().getType().equals("worker")) {
                            if (homeModel.getData().getStatus().equals("worker_finish")) {
                                Intent intent = new Intent(context, VerificationCodeActivity.class);
                                intent.putExtra("type", "order");
                                intent.putExtra("mobile", "");
                                intent.putExtra("orderId", String.valueOf(homeModel.getData().getId()));
                                context.startActivity(intent);
                                Bungee.split(context);

                            } else {
                                Intent intent = new Intent(context, HomeWorkerActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                                Bungee.split(context);

                            }

                        } else {
                            popAddRate.show();
                        }
                    } else {
                        errorToast(context, homeModel.getMsg());
                    }

                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }

            }

            @Override
            public void onError(Throwable t) {
                handleApiException(context, t);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }
}
