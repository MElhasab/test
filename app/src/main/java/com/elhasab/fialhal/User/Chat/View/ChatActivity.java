package com.elhasab.fialhal.User.Chat.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.ChatHistory.Presenter.ChatHistoryPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.rvMessages)
    ShimmerRecyclerView rvMessages;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;

    SharedPrefManager sharedPrefManager;
    ChatHistoryPresenter chatHistoryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        chatHistoryPresenter = new ChatHistoryPresenter(this, sharedPrefManager);
        chatHistoryPresenter.chat(rvMessages, pbPagination);
    }

}