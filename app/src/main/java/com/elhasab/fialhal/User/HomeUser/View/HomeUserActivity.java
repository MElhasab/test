package com.elhasab.fialhal.User.HomeUser.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.Presenter.HomeUserPresenter;
import com.elhasab.fialhal.User.Notifications.View.NotificationActivity;
import com.elhasab.fialhal.Utils.MenuListFragment.View.MenuListFragment;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import java.util.ArrayList;
import java.util.Locale;

public class HomeUserActivity extends ParentClass {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.rvCategories)
    ShimmerRecyclerView rvCategories;
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    String categoriesId = "";
    ArrayList<String> items = new ArrayList<>();

    FlowingDrawer mDrawer;
    HomeUserPresenter homeUserPresenter;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getLang(this).equals("en")) {
            setContentView(R.layout.activity_home_user_nav_left);
            ButterKnife.bind(this);
        }
        if (getLang(this).equals("ar")) {
            setContentView(R.layout.activity_home_user_nav_right);
            ButterKnife.bind(this);
        }
        setupMenu();
        initUI();
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        }
    }

    public void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        homeUserPresenter = new HomeUserPresenter(this, sharedPrefManager);
        homeUserPresenter.getCategory(rvCategories, tvConfirm, categoriesId, items);
        homeUserPresenter.setSetting();
        mDrawer = (FlowingDrawer) findViewById(R.id.drawerlayout);
        mDrawer.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {

            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {

            }
        });
    }


    @OnClick(R.id.ivNotifications)
    void notification() {
        Intent intent = new Intent(HomeUserActivity.this, NotificationActivity.class);
        startActivity(intent);
        Bungee.split(HomeUserActivity.this);
    }

    public void setupMenu() {
        FragmentManager fm = getSupportFragmentManager();
        MenuListFragment mMenuFragment = (MenuListFragment) fm.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fm.beginTransaction().add(R.id.id_container_menu, mMenuFragment).commit();
        }
    }

    @OnClick(R.id.ivMenu)
    void openMenu() {
        mDrawer.openMenu();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        } else {
            super.onBackPressed();
            finish();
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocal();
    }

    private void setLocal() {
        Locale locale = new Locale(ParentClass.getLang(this));
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawer.isMenuVisible()) {
            mDrawer.closeMenu();
        }
    }
}