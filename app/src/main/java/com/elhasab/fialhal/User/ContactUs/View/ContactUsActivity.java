package com.elhasab.fialhal.User.ContactUs.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.ContactUs.Presenter.ContactUsPresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class ContactUsActivity extends ParentClass {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvMobileNumber)
    TextView tvMobileNumber;
    @BindView(R.id.tvGmail)
    TextView tvGmail;
    @BindView(R.id.etName)
    EditText etNmae;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.tvSend)
    TextView tvSend;

    SharedPrefManager sharedPrefManager;
    ContactUsPresenter contactUsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        dismiss_keyboard();
        sharedPrefManager = new SharedPrefManager(this);
        sharedPrefManager = new SharedPrefManager(this);
        if (getLang(ContactUsActivity.this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right_white);
        } else {
            ivBack.setImageResource(R.mipmap.back_left_white);
        }

        tvMobileNumber.setText(sharedPrefManager.getUserDate().getMobile());
        tvGmail.setText(sharedPrefManager.getUserDate().getEmail());
        etNmae.setText(sharedPrefManager.getUserDate().getUsername());
        etMobile.setText(sharedPrefManager.getUserDate().getMobile());
        etMobile.setText(sharedPrefManager.getUserDate().getEmail());
        contactUsPresenter = new ContactUsPresenter(this, sharedPrefManager);
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
    }

    @OnClick(R.id.tvSend)
    void contact() {
        contactUsPresenter.contactUs(etNmae, etMobile, etEmail, etDescription);
    }

}