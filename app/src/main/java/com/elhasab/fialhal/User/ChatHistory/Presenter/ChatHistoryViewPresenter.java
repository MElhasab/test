package com.elhasab.fialhal.User.ChatHistory.Presenter;

import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface ChatHistoryViewPresenter {
    void chat(ShimmerRecyclerView rvMessages, ProgressBar pbPagination);
}
