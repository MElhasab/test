package com.elhasab.fialhal.User.Wallet.Presenter;

import android.widget.ProgressBar;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface WalletViewPresenter {
    void transfer(ShimmerRecyclerView recyclerView, ProgressBar pbPagination);
}
