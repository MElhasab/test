package com.elhasab.fialhal.User.Profile.Presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.RegisterStepTwo.View.RegisterStepTwoActivity;
import com.elhasab.fialhal.Models.CountriesModel.CountriesModel;
import com.elhasab.fialhal.Models.GeneralSpinnner;
import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import spencerstudios.com.bungeelib.Bungee;

public class ProfilePresenter extends BasePresenter implements ProfileViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    ArrayList<String> countryNames;
    ArrayList<Integer> countryIds;
    public String countryName = "";
    public int countryId = 0;

    public ProfilePresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getCountries(Spinner spCountries, String cityId) {
        countryNames = new ArrayList<>();
        countryIds = new ArrayList<>();
        countryIds.add(0);
        countryNames.add(context.getString(R.string.chooseCountry));

        Observable observable = RetroWeb.getClient().create(MainServices.class).countries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<CountriesModel> observer = new Observer<CountriesModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CountriesModel countriesModel) {
                try {
                    if (countriesModel.isValue()) {
                        for (int i = 0; i < countriesModel.getData().size(); i++) {
                            GeneralSpinnner generalSpinnner = new GeneralSpinnner();
                            generalSpinnner.setId(String.valueOf(countriesModel.getData().get(i).getId()));
                            generalSpinnner.setName(countriesModel.getData().get(i).getName());

                            countryIds.add(Integer.valueOf(generalSpinnner.getId()));
                            countryNames.add(generalSpinnner.getName());
                        }
                    }
                } catch (Exception e) {

                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.text_spinner, countryNames) {
                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);
                        if (((TextView) v).getText().equals(context.getString(R.string.chooseCountry))) {
                            ((TextView) v).setTextColor(Color.parseColor("#AAB5BC"));
                        } else {
                            ((TextView) v).setTextColor(Color.parseColor("#000000"));
                        }
                        return v;
                    }

                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            tv.setTextColor(Color.parseColor("#000000"));
                        } else {
                            tv.setTextColor(Color.parseColor("#000000"));
                        }
                        return view;
                    }
                };

                adapter.setDropDownViewResource(R.layout.text_spinner);

                spCountries.setAdapter(adapter);
                spCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position != 0) {
                            countryName = countryNames.get(position);
                            countryId = countryIds.get(position);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    @Override
    public void updateProfile(EditText etUsername, EditText etEmail, EditText etMobile, MultipartBody.Part multipartBodyImage,
                              String cityId, Boolean imageSelected, EditText experience) {
        Boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            etMobile.setError(context.getString(R.string.mobileNum));
            focusView = etMobile;
            cancel = true;
        }
        if (TextUtils.isEmpty(etUsername.getText().toString())) {
            etUsername.setError(context.getString(R.string.username));
            focusView = etUsername;
            cancel = true;
        }
        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError(context.getString(R.string.email));
            focusView = etEmail;
            cancel = true;
        }
        if (cancel) {

        } else {
            if (imageSelected) {
                startLoading();
                Observable observable = RetroWeb.getClient().create(MainServices.class).updateProfileWithImage(
                        Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), etUsername.getText().toString()
                        , etEmail.getText().toString(), etMobile.getText().toString(), cityId, multipartBodyImage)
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread());
                Observer<UserModel> observer = new Observer<UserModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserModel userModel) {
                        stopLoading();
                        if (userModel != null) {
                            if (userModel.isValue()) {
                                sharedPrefManager.setUserDate(userModel.getData());
                                if (sharedPrefManager.getUserDate().getType().equals("client")) {
                                    Intent intent = new Intent(context, HomeUserActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    context.startActivity(intent);
                                    Bungee.split(context);
                                } else {
                                    Intent intent = new Intent(context, HomeWorkerActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    context.startActivity(intent);
                                    Bungee.split(context);
                                }
                            } else {
                                errorToast(context, context.getString(R.string.someThingWentWrong));
                            }
                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleApiException(context, e);
                        stopLoading();
                    }

                    @Override
                    public void onComplete() {

                    }
                };
                observable.subscribe(observer);

            } else {
                startLoading();
                Observable observable = RetroWeb.getClient().create(MainServices.class).updateProfileWithoutImage(
                        Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), etUsername.getText().toString()
                        , etEmail.getText().toString(), etMobile.getText().toString(), cityId, experience.getText().toString())
                        .observeOn(Schedulers.io())
                        .subscribeOn(AndroidSchedulers.mainThread());
                Observer<UserModel> observer = new Observer<UserModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserModel userModel) {
                        stopLoading();
                        if (userModel != null) {
                            if (userModel.isValue()) {
                                sharedPrefManager.setUserDate(userModel.getData());
                                if (sharedPrefManager.getUserDate().getType().equals("client")) {
                                    Intent intent = new Intent(context, HomeUserActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    context.startActivity(intent);
                                    Bungee.split(context);
                                } else {
                                    Intent intent = new Intent(context, HomeWorkerActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    context.startActivity(intent);
                                    Bungee.split(context);
                                }
                            } else {
                                errorToast(context, context.getString(R.string.someThingWentWrong));
                            }
                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleApiException(context, e);
                        stopLoading();
                    }

                    @Override
                    public void onComplete() {

                    }
                };
                observable.subscribe(observer);
            }

        }
    }

}
