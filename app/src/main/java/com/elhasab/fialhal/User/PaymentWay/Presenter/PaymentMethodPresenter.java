package com.elhasab.fialhal.User.PaymentWay.Presenter;

import android.content.Context;
import android.content.Intent;

import com.elhasab.fialhal.Models.SingleOrderModel.SingleOrderModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class PaymentMethodPresenter extends BasePresenter implements PaymentMethodViewPresenter {

    Context context;
    SharedPrefManager sharedPrefManager;

    public PaymentMethodPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void paymentMethod(String id, String type) {
        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).pay_order(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), id, type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SingleOrderModel singleOrderModel) {
                stopLoading();
                if (singleOrderModel != null) {
                    if (singleOrderModel.isValue()) {
                        successToast(context, context.getString(R.string.choosenSuccefully));
                        Intent intent = new Intent(context, HomeUserActivity.class);
                        intent.putExtra("id", String.valueOf(singleOrderModel.getData().getId()));
                        context.startActivity(intent);
                        Bungee.split(context);
                    } else {
                        errorToast(context, singleOrderModel.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                handleApiException(context, e);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }
}
