package com.elhasab.fialhal.User.Profile.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.NewPassword.View.NewPasswordActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Profile.Presenter.ProfilePresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import java.io.File;
import java.io.IOException;

public class ProfileActivity extends ParentClass {

    @BindView(R.id.rlEditProfile)
    RelativeLayout rlEditProfile;
    @BindView(R.id.rlEditSection)
    RelativeLayout rlEditCategory;
    @BindView(R.id.rlChangePass)
    RelativeLayout rlChangePass;
    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.llPersonalInformation)
    RelativeLayout llPersonalInformation;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.spCountries)
    Spinner spCountries;
    @BindView(R.id.etExperience)
    EditText etExperience;
    @BindView(R.id.rlExperience)
    RelativeLayout rlExperience;
    @BindView(R.id.tvUpdate)
    TextView tvUpdate;
    @BindView(R.id.ivUserImage)
    ImageView ivUserImage;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    SharedPrefManager sharedPrefManager;
    ProfilePresenter profilePresenter;

    private static int PICK_IMAGE_REQUEST = 1;
    public boolean selected_image = false;
    MultipartBody.Part multipartImage;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        if (getIntent().getStringExtra("type").equals("worker")) {
            rlEditCategory.setVisibility(View.VISIBLE);
            rlExperience.setVisibility(View.VISIBLE);
        }
        sharedPrefManager = new SharedPrefManager(this);
        tvUsername.setText(sharedPrefManager.getUserDate().getUsername());
        etUsername.setText(sharedPrefManager.getUserDate().getUsername());
        etEmail.setText(sharedPrefManager.getUserDate().getEmail());
        etMobile.setText(sharedPrefManager.getUserDate().getMobile());
        etExperience.setText(sharedPrefManager.getUserDate().getExperience());
        if (sharedPrefManager.getUserDate().getImage().equals("")) {
            ivUserImage.setImageResource(R.drawable.default_image);
        } else {
            LoadImageWithPicasso(sharedPrefManager.getUserDate().getImage(), this, ivUserImage);
        }
        profilePresenter = new ProfilePresenter(this, sharedPrefManager);
        profilePresenter.getCountries(spCountries, String.valueOf(sharedPrefManager.getUserDate().getCountryId()));
    }

    @OnClick(R.id.tvUpdate)
    void update() {
        profilePresenter.updateProfile(etUsername, etEmail, etMobile, multipartImage, String.valueOf(profilePresenter.countryId),
                selected_image, etExperience);
    }

    @OnClick(R.id.cImage)
    void rlImage() {
        openGallery();
    }

    @OnClick(R.id.rlEditProfile)
    void editProfile() {
        rlEditProfile.setVisibility(View.GONE);
        rlChangePass.setVisibility(View.GONE);
        llPersonalInformation.setVisibility(View.VISIBLE);
        ivEdit.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.rlChangePass)
    void rlChangePass() {
        Intent intent = new Intent(this, NewPasswordActivity.class);
        startActivity(intent);
        Bungee.split(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && null != data) {
            selected_image = true;
            File file = new File(getRealPathFromUri(ProfileActivity.this, data.getData()));
            final Uri imageUri = data.getData();
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            multipartImage = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                ivUserImage.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
    }

    public String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @OnClick(R.id.ivBack)
    void ivBack() {
        finish();
        Bungee.split(this);
    }
}