package com.elhasab.fialhal.User.UserOrders.Presenter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.AllOrdersModel.AllOrdersModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.Network.MainURL;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.UserOrdersAdapter;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UserOrdersActivityPresenter extends BasePresenter implements UserOrdersActivityViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    int visibleItemCount;
    int totalItemCount;
    int pastVisiblesItems;
    String nextUrl;
    Boolean loading = false;

    String order = MainURL.client_orders;
    String workerOrder = MainURL.worker_orders;

    public UserOrdersActivityPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getOrders(ShimmerRecyclerView rvOrders, ProgressBar pbPagination, String type) {
        UserOrdersAdapter adapter = new UserOrdersAdapter(context, rvOrders);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvOrders.setLayoutManager(linearLayoutManager);
        rvOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pbPagination.setVisibility(View.VISIBLE);
                            Observable observable = RetroWeb.getClient().create(MainServices.class).orders(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), nextUrl)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread());
                            Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
                                @Override
                                public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                                }

                                @Override
                                public void onNext(@io.reactivex.annotations.NonNull AllOrdersModel allOrdersModel) {
                                    pbPagination.setVisibility(View.GONE);
                                    if (allOrdersModel != null) {
                                        if (allOrdersModel.isValue()) {

                                            adapter.addAll(allOrdersModel.getData());
                                            adapter.notifyDataSetChanged();
                                            nextUrl = String.valueOf(allOrdersModel.getLinks().getNext());
                                            try {
                                                loading = !nextUrl.equals(null);

                                            } catch (Exception e) {

                                            }
                                        } else {
                                            errorToast(context, allOrdersModel.getMsg());
                                            recyclerView.setVisibility(View.GONE);
                                        }
                                    } else {
                                        errorToast(context, context.getString(R.string.someThingWentWrong));
                                        recyclerView.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                                    errorToast(context, e.toString());
                                    handleApiException(context, e);
                                    pbPagination.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.GONE);
                                }

                                @Override
                                public void onComplete() {

                                }
                            };
                            observable.subscribe(observer);
                        }
                    }
                }
            }
        });

        Observable observable = RetroWeb.getClient().create(MainServices.class).orders(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), order + "/" + type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
            @Override
            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.annotations.NonNull AllOrdersModel allOrdersModel) {
                if (allOrdersModel != null) {
                    if (allOrdersModel.isValue()) {
                        rvOrders.hideShimmerAdapter();
                        adapter.addAll(allOrdersModel.getData());
                        adapter.notifyDataSetChanged();
                        nextUrl = String.valueOf(allOrdersModel.getLinks().getNext());
                        if (allOrdersModel.getData().size() == 0) {
                            rvOrders.setVisibility(View.GONE);
                        } else {
                            rvOrders.setVisibility(View.VISIBLE);
                        }
                            try {
                                loading = !nextUrl.equals(null);

                            } catch (Exception e) {

                            }
                        } else{
                            errorToast(context, allOrdersModel.getMsg());
                            rvOrders.setVisibility(View.GONE);
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                        rvOrders.setVisibility(View.GONE);
                    }
                }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                errorToast(context, e.toString());
                handleApiException(context, e);
                rvOrders.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        rvOrders.setAdapter(adapter);
    }


    @Override
    public void getWorkerOrders(ShimmerRecyclerView rvOrders, ProgressBar pbPagination, String type) {
        UserOrdersAdapter adapter = new UserOrdersAdapter(context, rvOrders);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvOrders.setLayoutManager(linearLayoutManager);
        rvOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            pbPagination.setVisibility(View.VISIBLE);
                            Observable observable = RetroWeb.getClient().create(MainServices.class).worker_orders(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), nextUrl)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread());
                            Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
                                @Override
                                public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                                }

                                @Override
                                public void onNext(@io.reactivex.annotations.NonNull AllOrdersModel allOrdersModel) {
                                    pbPagination.setVisibility(View.GONE);
                                    if (allOrdersModel != null) {
                                        if (allOrdersModel.isValue()) {

                                            adapter.addAll(allOrdersModel.getData());
                                            adapter.notifyDataSetChanged();
                                            nextUrl = String.valueOf(allOrdersModel.getLinks().getNext());
                                            try {
                                                loading = !nextUrl.equals(null);

                                            } catch (Exception e) {

                                            }
                                        } else {
                                            errorToast(context, allOrdersModel.getMsg());
                                            recyclerView.setVisibility(View.GONE);
                                        }
                                    } else {
                                        errorToast(context, context.getString(R.string.someThingWentWrong));
                                        recyclerView.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                                    errorToast(context, e.toString());
                                    handleApiException(context, e);
                                    pbPagination.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.GONE);
                                }

                                @Override
                                public void onComplete() {

                                }
                            };
                            observable.subscribe(observer);
                        }
                    }
                }
            }
        });

        Observable observable = RetroWeb.getClient().create(MainServices.class).orders(Constants.beforApiToken + sharedPrefManager.getUserDate().getToken(), workerOrder + "/" + type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<AllOrdersModel> observer = new Observer<AllOrdersModel>() {
            @Override
            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

            }

            @Override
            public void onNext(@io.reactivex.annotations.NonNull AllOrdersModel allOrdersModel) {
                if (allOrdersModel != null) {
                    if (allOrdersModel.isValue()) {
                        rvOrders.hideShimmerAdapter();
                        adapter.addAll(allOrdersModel.getData());
                        adapter.notifyDataSetChanged();
                        nextUrl = String.valueOf(allOrdersModel.getLinks().getNext());
                        if (allOrdersModel.getData().size() == 0) {
                            rvOrders.setVisibility(View.GONE);
                        } else {
                            rvOrders.setVisibility(View.VISIBLE);
                        }
                        try {
                            loading = !nextUrl.equals(null);

                        } catch (Exception e) {

                        }
                    } else{
                        errorToast(context, allOrdersModel.getMsg());
                        rvOrders.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    rvOrders.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                errorToast(context, e.toString());
                handleApiException(context, e);
                rvOrders.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        rvOrders.setAdapter(adapter);
    }
}
