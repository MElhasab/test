package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Models.MainCategoryAndItsSub.SubCategory;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.AddRequest.View.AddRequestActivity;
import com.elhasab.fialhal.Utils.ParentClass;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserSubCategoryAdapter extends RecyclerView.Adapter<UserSubCategoryAdapter.ViewHolder> {
    Context context;
    ArrayList<SubCategory> homeList;
    LayoutInflater layoutInflater;
    ArrayList<String> items;
    String categotiesId;
    TextView tvConfirm;

    public UserSubCategoryAdapter(Context context, String categotiesId, ArrayList<String> items, TextView tvConfirm) {
        this.context = context;
        homeList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items = items;
        this.categotiesId = categotiesId;
        this.tvConfirm = tvConfirm;
    }

    @NonNull
    @Override
    public UserSubCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_user_sub_category, parent, false);
        return new UserSubCategoryAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull UserSubCategoryAdapter.ViewHolder holder, int position) {
        holder.tvTitle.setText(homeList.get(position).getName());
        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    items.add(String.valueOf(homeList.get(position).getId()));
                } else if ((!isChecked)) {
                    items.remove(String.valueOf(homeList.get(position).getId()));
                }
            }
        });
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categotiesId = TextUtils.join(",", items);
                if (categotiesId.equals("")) {
                    ParentClass.makeErrorToast(context, context.getString(R.string.chooseAtLeastOneCategory));
                } else {
                    Intent intent = new Intent(context, AddRequestActivity.class);
                    intent.putExtra("categoriesId", categotiesId);
                    context.startActivity(intent);
                }
            }
        });

        if (!homeList.get(position).getImage().equals("")) {
            ParentClass.LoadImageWithPicasso(homeList.get(position).getImage(), context, holder.ivSubCategory);
        } else {
            holder.ivSubCategory.setImageResource(R.drawable.default_image);
        }
    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<SubCategory> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDiscription)
        TextView tvDiscription;
        @BindView(R.id.checkBox)
        CheckBox checkBox;
        @BindView(R.id.ivSubCategory)
        ImageView ivSubCategory;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
