package com.elhasab.fialhal.User.PaymentWay.View;

import android.os.Bundle;
import android.widget.ImageView;

import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.PaymentWay.Presenter.PaymentMethodPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentMethodActivity extends AppCompatActivity {

    @BindView(R.id.ivRadioDelivary)
    ImageView ivDelivary;
    @BindView(R.id.ivRadioVisa)
    ImageView ivVisa;
    @BindView(R.id.ivRadioWallet)
    ImageView ivWallet;

    String type = "cash";
    SharedPrefManager sharedPrefManager;
    PaymentMethodPresenter paymentMethodPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        paymentMethodPresenter = new PaymentMethodPresenter(this, sharedPrefManager);
    }

    @OnClick(R.id.rlPayment)
    void dlivary() {
        if (type.equals("cash")) {
            ivDelivary.setImageResource(R.mipmap.radiobutton_full);
            ivVisa.setImageResource(R.mipmap.radio_empty);
            ivWallet.setImageResource(R.mipmap.radio_empty);
            type = "cash";
        }
    }

    @OnClick(R.id.rlVisa)
    void visa() {
        if (type.equals("visa")) {
            ivDelivary.setImageResource(R.mipmap.radio_empty);
            ivVisa.setImageResource(R.mipmap.radiobutton_full);
            ivWallet.setImageResource(R.mipmap.radio_empty);
            type = "visa";
        }
    }

    @OnClick(R.id.rlWallet)
    void wallet() {
        if (type.equals("wallet")) {
            ivDelivary.setImageResource(R.mipmap.radio_empty);
            ivVisa.setImageResource(R.mipmap.radio_empty);
            ivWallet.setImageResource(R.mipmap.radiobutton_full);
            type = "wallet";
        }
    }

    @OnClick(R.id.tvConfirm)
    void confirm() {
        paymentMethodPresenter.paymentMethod(getIntent().getStringExtra("id"), type);
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
    }
}