package com.elhasab.fialhal.User.AddRequest.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import spencerstudios.com.bungeelib.Bungee;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.ImagesAdapter;
import com.elhasab.fialhal.User.AddRequest.Presenter.AddRequestPresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.Map.View.MapActivity;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddRequestActivity extends ParentClass {

    SharedPrefManager sharedPrefManager;
    AddRequestPresenter addRequestPresenter;
    List<Bitmap> imageModelList = new ArrayList<>();
    ArrayList<File> files = new ArrayList<>();
    File file;
    ArrayList<Uri> imageList = new ArrayList<>();
    List<MultipartBody.Part> parts = new ArrayList<>();
    List<URL> imagesUrl = new ArrayList<>();
    boolean selectedList = false;


    @BindView(R.id.rlAddImage)
    RelativeLayout rlImages;
    @BindView(R.id.rvUploadImages)
    RecyclerView rvUploadImages;
    @BindView(R.id.etOrderTitle)
    EditText etRequestTitle;
    @BindView(R.id.etOrderDescription)
    EditText etRequestDescription;
    @BindView(R.id.spCountries)
    Spinner spCities;
    @BindView(R.id.rlMap)
    RelativeLayout rlMap;
    @BindView(R.id.tvAddressOnMap)
    TextView tvAddressOnMap;
    @BindView(R.id.tvLat)
    TextView tvLat;
    @BindView(R.id.tvLng)
    TextView tvLng;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvStartTime)
    TextView tvStartTime;
    @BindView(R.id.tvEndTime)
    TextView tvEndtime;
    String address = "";
    String date = "";
    String chooseTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        addRequestPresenter = new AddRequestPresenter(this, sharedPrefManager);
        addRequestPresenter.getCities(spCities, String.valueOf(sharedPrefManager.getUserDate().getCountryId()));
    }

    @OnClick(R.id.tvDate)
    void date() {
        Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.MyDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                date = year + "-" + (month + 1) + "-" + dayOfMonth;
                tvDate.setText(date);
            }
        }, y, m, d);
        datePickerDialog.show();
    }

    @OnClick(R.id.tvStartTime)
    void startTime() {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DialogDate, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String format = "";
                if (hourOfDay == 0) {
                    hourOfDay += 12;
                    format = "AM";
                } else if (hourOfDay == 12) {
                    format = "PM";
                } else if (hourOfDay > 12) {
                    hourOfDay -= 12;
                    format = "PM";
                } else {
                    format = "AM";
                }

                chooseTime = (hourOfDay + ":" + minute + format);
                tvStartTime.setText(chooseTime);
            }
        }, hour, minute, false);

        timePickerDialog.show();
    }

    @OnClick(R.id.tvEndTime)
    void endTime() {
        Calendar currentTime = Calendar.getInstance();
        int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        int minute = currentTime.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, R.style.DialogDate, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String format = "";
                if (hourOfDay == 0) {

                    hourOfDay += 12;

                    format = "AM";
                } else if (hourOfDay == 12) {

                    format = "PM";
                } else if (hourOfDay > 12) {

                    hourOfDay -= 12;

                    format = "PM";
                } else {

                    format = "AM";
                }

                chooseTime = (hourOfDay + ":" + minute + format);
                tvStartTime.setText(chooseTime);
            }
        }, hour, minute, false);

        timePickerDialog.show();
    }

    @OnClick(R.id.rlMap)
    void defineOnMap() {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
        Bungee.split(this);
    }

    @OnClick(R.id.rlAddImage)
    void addImage() {
        Intent intent = new Intent(this, AlbumSelectActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 10);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            ImagesAdapter adapter = new ImagesAdapter(this, imageList, selectedList);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
            rvUploadImages.setLayoutManager(layoutManager);
            selectedList = true;
//            ImageList = new ArrayList<>();
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
            if (!(images.size() + imageList.size() > 10)) {
                for (int i = 0; i < images.size(); i++) {
                    file = new File(images.get(i).path);
                    final Uri imageUri = Uri.fromFile(file);
                    files.add(file);
                    Bitmap myBitmap = BitmapFactory.decodeFile(files.get(i).getAbsolutePath());
                    imageModelList.add(myBitmap);
                    imageList.add(imageUri);
                    try {
                        imagesUrl.add(new URL(imageUri.toString()));
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                adapter.addAll(imageList);
                adapter.notifyDataSetChanged();
                rvUploadImages.setAdapter(adapter);
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(fileUri.getPath());

        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/*"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @OnClick(R.id.tvAdd)
    void addOrder() {
        parts.clear();
        for (int i = 0; i < imageList.size(); i++) {
            parts.add(prepareFilePart("images[]", imageList.get(i)));
        }
        addRequestPresenter.addRequest(etRequestTitle, etRequestDescription, String.valueOf(addRequestPresenter.cityId), sharedPrefManager.getLat()
                , sharedPrefManager.getLng(), getIntent().getStringExtra("categoriesId")
                , tvDate, tvStartTime, tvEndtime, address, parts, selectedList);
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
        Bungee.split(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(sharedPrefManager.getLat().equals(""))) {
            address = ParentClass.getCountryName(this
                    , Double.parseDouble(sharedPrefManager.getLat())
                    , Double.parseDouble(sharedPrefManager.getLat()));
        }
    }
}