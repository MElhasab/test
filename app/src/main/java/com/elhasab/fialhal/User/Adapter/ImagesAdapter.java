package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.elhasab.fialhal.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {
    Context context;
    List<Uri> imageList;
    LayoutInflater layoutInflater;
    ArrayList<Uri> imageListMain;

    Boolean selected;

    public ImagesAdapter(Context context, ArrayList<Uri> imageListMain, Boolean selected) {
        this.context = context;
        imageList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageListMain = imageListMain;
        this.selected = selected;
    }

    @NonNull
    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_images, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImagesAdapter.ViewHolder holder, int position) {
        Picasso.with(context).load(imageList.get(position)).into(holder.ivImage);
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageList.remove(position);
                notifyItemRemoved(position);
                notifyItemMoved(position, imageList.size());
                notifyDataSetChanged();
                imageListMain.remove(position);
                if (imageListMain.size() == 0) {
                    selected = false;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (imageList.size() == 0) {
            return 10;
        } else {
            return imageList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Uri> data) {
        imageList.addAll(data);
        notifyDataSetChanged();

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivImage)
        ImageView ivImage;
        @BindView(R.id.ivRemove)
        ImageView ivRemove;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
