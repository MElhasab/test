package com.elhasab.fialhal.User.AddRequest.Presenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.elhasab.fialhal.Models.CitiesModel.CitiesModel;
import com.elhasab.fialhal.Models.GeneralSpinnner;
import com.elhasab.fialhal.Models.OrderModel.OrderModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import spencerstudios.com.bungeelib.Bungee;

public class AddRequestPresenter extends BasePresenter implements AddRequestViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    ArrayList<String> citiesNames;
    ArrayList<Integer> citiesIds;
    public int cityId = 0;
    public String cityName = "";


    public AddRequestPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;

    }

    @Override
    public void getCities(Spinner spCity, String id) {
        citiesIds = new ArrayList<>();
        citiesNames = new ArrayList<>();
        citiesNames.add(context.getString(R.string.citiesNames));
        citiesIds.add(0);

        Observable observable = RetroWeb.getClient().create(MainServices.class).cities()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<CitiesModel> observer = new Observer<CitiesModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CitiesModel citiesModel) {
                if (citiesModel.isValue()) {
                    for (int i = 0; i < citiesModel.getData().size(); i++) {
                        GeneralSpinnner generalSpinnner = new GeneralSpinnner();
                        generalSpinnner.setName(citiesModel.getData().get(i).getName());
                        generalSpinnner.setId(String.valueOf(citiesModel.getData().get(i).getId()));
                        citiesNames.add(generalSpinnner.getName());
                        citiesIds.add(Integer.valueOf(generalSpinnner.getId()));
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.text_spinner, citiesNames) {

                    @NonNull
                    @Override
                    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View v = super.getView(position, convertView, parent);

                        if (((TextView) v).getText().equals(context.getString(R.string.citiesNames))) {
                            ((TextView) v).setTextColor(Color.parseColor("#E2A000"));
                        } else {
                            ((TextView) v).setTextColor(Color.parseColor("#000000"));
                        }
                        return v;
                    }

                    @Override
                    public boolean isEnabled(int position) {
                        if (position == 0) {
                            return false;
                        } else {
                            return true;
                        }
                    }

                    @Override
                    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                        View view = super.getDropDownView(position, convertView, parent);
                        TextView tv = (TextView) view;
                        if (position == 0) {
                            tv.setTextColor(Color.parseColor("#000000"));
                        } else {
                            tv.setTextColor(Color.parseColor("#000000"));
                        }
                        return view;
                    }

                };
                adapter.setDropDownViewResource(R.layout.text_spinner);

                spCity.setAdapter(adapter);
                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position != 0) {
                            cityName = citiesNames.get(position);
                            cityId = citiesIds.get(position);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }

    @Override
    public void addRequest(EditText etTitle, EditText etDescription, String cityId, String lat, String lng
            , String categoriesId, TextView date, TextView startTime, TextView endTime, String address
            , List<MultipartBody.Part> images, Boolean selected) {
        View focusView = null;
        Boolean cancel = false;
        if (TextUtils.isEmpty(etTitle.getText().toString())) {
            etTitle.setError(context.getString(R.string.orderAddress));
            cancel = true;
        }
        if (TextUtils.isEmpty(etDescription.getText().toString())) {
            etDescription.setError(context.getString(R.string.description));
            cancel = true;
        }
        if (cityId.equals("0")) {
            errorToast(context, context.getString(R.string.citiesNames));
            cancel = true;

        }
        if (lat.equals("")) {
            errorToast(context, context.getString(R.string.locationOnMap));
            cancel = true;
        }
        if (date.getText().toString().equals("")) {
            errorToast(context, context.getString(R.string.date));
            cancel = true;
        }
        if (startTime.getText().toString().equals("")) {
            errorToast(context, context.getString(R.string.startTime));
            cancel = true;
        }
        if (endTime.getText().toString().equals("")) {
            errorToast(context, context.getString(R.string.endTime));
            cancel = true;
        }
        if (!selected) {
            errorToast(context, context.getString(R.string.requestPics));
            cancel = true;
        }
        if (cancel) {

        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).make_order(Constants.beforApiToken +
                            sharedPrefManager.getUserDate().getToken(), categoriesId, etTitle.getText().toString()
                    , etDescription.getText().toString(), cityId, lat, lng, address, date.getText().toString()
                    , startTime.getText().toString(), endTime.getText().toString(), images)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<OrderModel> observer = new Observer<OrderModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(OrderModel orderModel) {
                    stopLoading();
                    if (orderModel != null) {
                        if (orderModel.isValue()) {
                            successToast(context, context.getString(R.string.orderMadeSuccessfully));
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);
                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }

    }
}
