package com.elhasab.fialhal.User.PaymentWay.Presenter;

import android.widget.TextView;

public interface PaymentMethodViewPresenter {
    void paymentMethod(String id ,String type);
}
