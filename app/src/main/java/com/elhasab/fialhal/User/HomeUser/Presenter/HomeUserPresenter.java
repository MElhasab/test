package com.elhasab.fialhal.User.HomeUser.Presenter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.MainCategoryAndItsSub.MainCategoryAndItsSubModel;
import com.elhasab.fialhal.Models.SettingModel.SettingModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.Network.MainURL;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.UserCategoryAdapter;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeUserPresenter extends BasePresenter implements HomeUserViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public HomeUserPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void setSetting() {
        Observable observable = RetroWeb.getClient().create(MainServices.class).settings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SettingModel> observer = new Observer<SettingModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SettingModel settingModel) {
                if (settingModel != null) {
                    if (settingModel.isValue()) {
                        sharedPrefManager.setSettingsData(settingModel.getData());
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                errorToast(context, context.getString(R.string.someThingWentWrong));
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    @Override
    public void getCategory(ShimmerRecyclerView rvCategory, TextView tvConfirm, String categoriesId, ArrayList<String> items) {
        UserCategoryAdapter adapter = new UserCategoryAdapter(context, rvCategory, categoriesId, items, tvConfirm);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvCategory.setLayoutManager(layoutManager);
        Observable observable = RetroWeb.getClient().create(MainServices.class).main_categories_and_its_sub()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<MainCategoryAndItsSubModel> observer = new Observer<MainCategoryAndItsSubModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(MainCategoryAndItsSubModel response) {
                if (response != null) {
                    if (response.isValue()) {
                        rvCategory.hideShimmerAdapter();
                        adapter.addAll(response.getData());
                        adapter.notifyDataSetChanged();
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                        rvCategory.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    rvCategory.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                rvCategory.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        rvCategory.setAdapter(adapter);
    }


}
