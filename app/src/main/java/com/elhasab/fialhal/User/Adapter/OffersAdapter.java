package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.elhasab.fialhal.Models.OfferModel.Data;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.OrderDetails.View.OrderDetailsActivity;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {
    List<Data> homeList;
    Context context;
    LayoutInflater layoutInflater;
    SharedPrefManager sharedPrefManager;
    RecyclerView rvObjects;

    public OffersAdapter(Context context, SharedPrefManager sharedPrefManager, RecyclerView rvObjects) {
        this.context = context;
        homeList = new ArrayList<>();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sharedPrefManager = sharedPrefManager;
        this.rvObjects = rvObjects;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_offer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load(homeList.get(position).getWorkerImage()).into(holder.ownerImage);
        holder.tvOwnerName.setText(homeList.get(position).getWorkerUsername());
        holder.tvOwnerLocation.setText(homeList.get(position).getWorkerAddress());
        holder.tvOwnerRating.setText(homeList.get(position).getWorkerRate());
        holder.tvDate.setText(homeList.get(position).getTime());
        holder.tvOfferBody.setText(homeList.get(position).getComment());
        holder.tvOfferPrice.setText(homeList.get(position).getPrice());
        holder.tvWatchOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Data> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivOfferOwnerImage)
        ImageView ownerImage;
        @BindView(R.id.tvOfferOwnerName)
        TextView tvOwnerName;
        @BindView(R.id.tvOfferOwnerLocation)
        TextView tvOwnerLocation;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvOfferOwnerRating)
        TextView tvOwnerRating;
        @BindView(R.id.tvOfferPrice)
        TextView tvOfferPrice;
        @BindView(R.id.tvOfferBody)
        TextView tvOfferBody;
        @BindView(R.id.tvWatchOffer)
        TextView tvWatchOffer;
        @BindView(R.id.rlReport)
        RelativeLayout rlReport;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);
        }
    }
}
