package com.elhasab.fialhal.User.OrderDetails.Presenter;

import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Models.SingleOrderModel.SingleOrderModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class OrderDetailsPresenter extends BasePresenter implements OrderDetailsViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public OrderDetailsPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getOrderDetails(TextView tvOrderNumber, TextView tvUsername, TextView tvStatus, TextView tvWorkerName
            , TextView tvReservationNumper, TextView tvWorkerAddress, TextView tvDateAndTime, TextView tvPrice
            , ImageView ivProfileImage, GoogleMap gmap, String id, String position, TextView tvOfferId) {

        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).single_orders(Constants.beforApiToken
                + sharedPrefManager.getUserDate().getToken(), id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SingleOrderModel singleOrderModel) {
                stopLoading();
                if (singleOrderModel != null) {
                    if (singleOrderModel.isValue()) {
                        tvOrderNumber.setText(String.valueOf(singleOrderModel.getData().getCode()));
                        tvUsername.setText(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerUsername());
                        tvStatus.setText(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getStatus());
                        tvReservationNumper.setText(String.valueOf(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerUsername()));
                        tvWorkerAddress.setText(String.valueOf(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerAddress()));
                        tvDateAndTime.setText(String.valueOf(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getTime()));
                        tvPrice.setText(String.valueOf(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getPrice() +
                                " " + context.getString(R.string.sar)));
                        loadImage(singleOrderModel.getData().getImages().get(0).getImage(), context, ivProfileImage);

                        gmap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerLat())
                                , Double.parseDouble(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerLng())), 12.0f));
                        gmap.clear();
                        gmap.addMarker(new MarkerOptions().position(
                                new LatLng(Double.parseDouble(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerLat())
                                        , Double.parseDouble(singleOrderModel.getData().getOffers().get(Integer.parseInt(position)).getWorkerLng())))
                                .title("")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));

                    } else {
                        errorToast(context, singleOrderModel.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                stopLoading();
                handleApiException(context, e);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }

    @Override
    public void responseOffer(TextView tvOfferId, String type) {

        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).offer_response(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), tvOfferId.getText().toString(), type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SingleOrderModel singleOrderModel) {
                stopLoading();
                if (singleOrderModel != null) {
                    if (singleOrderModel.isValue()) {
                        if (type.equals("approve")) {
                            successToast(context, context.getString(R.string.approvedSuccefully));
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.putExtra("id", String.valueOf(singleOrderModel.getData().getId()));
                            context.startActivity(intent);
                            Bungee.split(context);
                        }
                        if (type.equals("decline")) {
                            successToast(context, context.getString(R.string.rejectedSuccefully));
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);
                        }
                    } else {
                        errorToast(context, singleOrderModel.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                handleApiException(context, e);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

}