package com.elhasab.fialhal.User.AddRequest.Presenter;

import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import okhttp3.MultipartBody;

public interface AddRequestViewPresenter {
    void getCities(Spinner spCity, String id);

    void addRequest(EditText etTitle, EditText etDescription, String cityId, String lat, String lng, String categoriesId, TextView date
            , TextView startTime, TextView endTime, String address, List<MultipartBody.Part> images, Boolean selected);
}
