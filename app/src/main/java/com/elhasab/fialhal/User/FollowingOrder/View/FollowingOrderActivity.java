package com.elhasab.fialhal.User.FollowingOrder.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.ForgetPassword.View.ForgetPasswordActivity;
import com.elhasab.fialhal.Authentication.VerificationCode.View.VerificationCodeActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.FollowingOrder.Presenter.FollowingOrderPresenter;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.hedgehog.ratingbar.RatingBar;

public class FollowingOrderActivity extends ParentClass {

    @BindView(R.id.viewSecondPhase)
    View viewSecondPhase;
    @BindView(R.id.viewThirdPhase)
    View viewThirdPhase;
    @BindView(R.id.ivThirdPhase)
    ImageView ivThirdPhase;
    @BindView(R.id.ivFourthPhase)
    ImageView ivFourthPhase;
    @BindView(R.id.tvBasedOnStatus)
    TextView tvBasedOnStatus;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    SharedPrefManager sharedPrefManager;
    FollowingOrderPresenter followingOrderPresenter;
    BottomSheetDialog popAddRate;
    ImageView ivClose;
    RatingBar ratingBar;
    EditText etAddComment;
    TextView tvSendRate;
    float rate = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_order);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        popAddRate = new BottomSheetDialog(this, R.style.DialogStyle);
        popAddRate.setContentView(R.layout.pop_up_add_rating);
        ivClose = (ImageView) popAddRate.findViewById(R.id.ivClose);
        etAddComment = (EditText) popAddRate.findViewById(R.id.etRating);
        ratingBar = (RatingBar) popAddRate.findViewById(R.id.ratingBar);
        tvSendRate = (TextView) popAddRate.findViewById(R.id.tvSendRate);
        ratingBar.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(float RatingCount) {
                rate = RatingCount;
            }
        });
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popAddRate.dismiss();
            }
        });
        sharedPrefManager = new SharedPrefManager(this);
        followingOrderPresenter = new FollowingOrderPresenter(this, sharedPrefManager);
        followingOrderPresenter.followingOrder(viewSecondPhase, viewThirdPhase, ivThirdPhase, ivFourthPhase,
                tvBasedOnStatus, tvStatus, getIntent().getStringExtra("id"));

        tvSendRate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followingOrderPresenter.rate(getIntent().getStringExtra("id"), String.valueOf(rate), etAddComment);
            }
        });
    }

    @OnClick(R.id.ivBack)
    void ivBack() {
        finish();
    }

    @OnClick(R.id.tvBasedOnStatus)
    void tvBasedOnStatus() {
        Log.e("tvStatus", tvStatus.getText().toString() + "GOOD");
        if (sharedPrefManager.getUserDate().getType().equals("worker")) {
            if (tvStatus.getText().toString().equals("in_progress")) {
                followingOrderPresenter.changeOrderStatus(getIntent().getStringExtra("id"), "worker_in_way", popAddRate);
            }
            if (tvStatus.getText().toString().equals("worker_in_way")) {
                followingOrderPresenter.changeOrderStatus(getIntent().getStringExtra("id"), "worker_finish", popAddRate);
            }
            if (tvStatus.getText().toString().equals("worker_finish")) {
                Intent intent = new Intent(FollowingOrderActivity.this, VerificationCodeActivity.class);
                intent.putExtra("type", "order");
                intent.putExtra("mobile", "");
                intent.putExtra("orderId", getIntent().getStringExtra("id"));
                startActivity(intent);
                Bungee.split(FollowingOrderActivity.this);
            }

        } else {
            if (tvStatus.getText().toString().equals("worker_in_way"))
                followingOrderPresenter.changeOrderStatus(getIntent().getStringExtra("id"), "client_finish", popAddRate);

        }
    }
}