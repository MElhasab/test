package com.elhasab.fialhal.User.RequestDetails.Presenter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.elhasab.fialhal.Models.GeneralResponse;
import com.elhasab.fialhal.Models.SingleOrderModel.SingleOrderModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.Adapter.OfferAdapter;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class RequestDetailsPresenter extends BasePresenter implements RequestDetailsViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public RequestDetailsPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getSingelOrder(SliderLayout sliderLayout, RelativeLayout relativeUpdate, RelativeLayout relativeDelete
            , TextView tvLocation, TextView tvTitle, TextView tvDate, TextView tvDescription
            , RelativeLayout relativeInfoForEveryStatusButInProgress, TextView tvPaymentWay, TextView tvTax
            , TextView tvTotal, TextView tvCancelOrder, ShimmerRecyclerView recyclerView, String id, String type
            , RelativeLayout relativeNoImage, TextView tvOffersTitle, RelativeLayout relativeContact, TextView tvCallNow
            , TextView tvContactNow, TextView tvTrackOrder, TextView tvMobile, TextView tvWorkerId, TextView tvWorkerName) {

        OfferAdapter adapter = new OfferAdapter(context, recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        Observable observable = RetroWeb.getClient().create(MainServices.class).single_offer(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull SingleOrderModel singleOrderModel) {
                if (singleOrderModel != null) {
                    if (singleOrderModel.isValue()) {
                        if (singleOrderModel.getData().getImages().size() > 0) {
                            sliderLayout.setVisibility(View.VISIBLE);
                            relativeNoImage.setVisibility(View.GONE);
                            for (int i = 0; i < singleOrderModel.getData().getImages().size(); i++) {

                                sliderLayout.addSlider(new TextSliderView(context).description("")
                                        .image(singleOrderModel.getData().getImages().get(i).getImage()));
                            }
                        } else {
                            sliderLayout.setVisibility(View.GONE);
                            relativeNoImage.setVisibility(View.VISIBLE);
                        }
                        tvDescription.setText(singleOrderModel.getData().getDescription());
                        tvTitle.setText(singleOrderModel.getData().getTitle());
                        tvWorkerName.setText(singleOrderModel.getData().getWorkerUsername());
                        tvDate.setText(singleOrderModel.getData().getDate());
                        tvLocation.setText(singleOrderModel.getData().getAddress());
                        tvMobile.setText(singleOrderModel.getData().getWorkerMobile());
                        tvPaymentWay.setText(singleOrderModel.getData().getPaymentType());
                        tvWorkerId.setText(String.valueOf(singleOrderModel.getData().getId()));
                        if (singleOrderModel.getData().getTotalPrice().equals(null)) {
                            tvTax.setText("0");
                            tvTotal.setText("0");
                        } else {
                            tvTax.setText(singleOrderModel.getData().getAdditionalFee());
                            tvTotal.setText(singleOrderModel.getData().getTotalPrice());
                        }
                        if (singleOrderModel.getData().getOffers().size() == 0) {
                            tvOffersTitle.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.VISIBLE);
                        }

                        if (type.equals("new")) {
                            relativeContact.setVisibility(View.GONE);
                            relativeDelete.setVisibility(View.VISIBLE);
                            relativeInfoForEveryStatusButInProgress.setVisibility(View.VISIBLE);
                            tvCancelOrder.setVisibility(View.VISIBLE);
                            relativeUpdate.setVisibility(View.GONE);
                            tvTrackOrder.setVisibility(View.GONE);
                        }
                        if (type.equals("in_progress")) {
                            relativeContact.setVisibility(View.VISIBLE);
                            relativeDelete.setVisibility(View.GONE);
                            relativeInfoForEveryStatusButInProgress.setVisibility(View.GONE);
                            tvCancelOrder.setVisibility(View.GONE);
                            relativeUpdate.setVisibility(View.GONE);
                            tvOffersTitle.setVisibility(View.GONE);
                            tvTrackOrder.setVisibility(View.VISIBLE);
                        }
                        if (type.equals("finish")) {
                            relativeContact.setVisibility(View.GONE);
                            relativeDelete.setVisibility(View.GONE);
                            relativeInfoForEveryStatusButInProgress.setVisibility(View.VISIBLE);
                            tvCancelOrder.setVisibility(View.GONE);
                            relativeUpdate.setVisibility(View.GONE);
                            tvOffersTitle.setVisibility(View.GONE);
                            tvTrackOrder.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        }
                        if (type.equals("canceled")) {
                            relativeContact.setVisibility(View.GONE);
                            relativeDelete.setVisibility(View.GONE);
                            relativeInfoForEveryStatusButInProgress.setVisibility(View.VISIBLE);
                            tvCancelOrder.setVisibility(View.GONE);
                            relativeUpdate.setVisibility(View.GONE);
                            tvOffersTitle.setVisibility(View.GONE);
                            tvTrackOrder.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.GONE);
                        }

                        recyclerView.hideShimmerAdapter();
                        adapter.addAll(singleOrderModel.getData().getOffers());
                        adapter.notifyDataSetChanged();

                    } else {
                        errorToast(context, singleOrderModel.getMsg());
                        recyclerView.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                errorToast(context, e.toString());
                handleApiException(context, e);
                recyclerView.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void cancelOrder(String id) {

        Observable observable = RetroWeb.getClient().create(MainServices.class).cancel_order(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), id)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
        Observer<GeneralResponse> observer = new Observer<GeneralResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull GeneralResponse generalResponse) {
                if (generalResponse != null) {
                    if (generalResponse.isValue()) {
                        successToast(context, generalResponse.getMsg());
                        if (sharedPrefManager.getUserDate().getType().equals("worker")) {
                            Intent intent = new Intent(context, HomeWorkerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);

                        } else {
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);

                        }
                    } else {
                        errorToast(context, generalResponse.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                handleApiException(context, e);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }

    @Override
    public void deleteOrder(String id) {
        Observable observable = RetroWeb.getClient().create(MainServices.class).delete_order(Constants.beforApiToken +
                sharedPrefManager.getUserDate().getToken(), id)
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread());
        Observer<GeneralResponse> observer = new Observer<GeneralResponse>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull GeneralResponse generalResponse) {
                if (generalResponse != null) {
                    if (generalResponse.isValue()) {
                        successToast(context, generalResponse.getMsg());
                        if (sharedPrefManager.getUserDate().getType().equals("worker")) {
                            Intent intent = new Intent(context, HomeWorkerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);

                        } else {
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);

                        }
                    } else {
                        errorToast(context, generalResponse.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                handleApiException(context, e);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }
}
