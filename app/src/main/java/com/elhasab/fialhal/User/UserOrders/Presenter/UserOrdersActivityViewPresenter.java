package com.elhasab.fialhal.User.UserOrders.Presenter;

import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface UserOrdersActivityViewPresenter {
    void getOrders(ShimmerRecyclerView rvOrders, ProgressBar pbPagination, String type);

    void getWorkerOrders(ShimmerRecyclerView rvOrders, ProgressBar pbPagination, String type);
}
