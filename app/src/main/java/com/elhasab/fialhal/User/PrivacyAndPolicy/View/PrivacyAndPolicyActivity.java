package com.elhasab.fialhal.User.PrivacyAndPolicy.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.TextView;

import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;

public class PrivacyAndPolicyActivity extends ParentClass {

    @BindView(R.id.tvBody)
    TextView tvBody;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.bind(this);
        tvBody.setText(sharedPrefManager.getSettingsData().getPolicy());
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
    }
}