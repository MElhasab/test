package com.elhasab.fialhal.User.ChatHistory.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.ChatHistory.Presenter.ChatHistoryPresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import static com.elhasab.fialhal.Utils.ParentClass.getLang;

public class ChatHistoryActivity extends AppCompatActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rvMessages)
    ShimmerRecyclerView rvMessages;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;

    SharedPrefManager sharedPrefManager;
    ChatHistoryPresenter chatHistoryPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_history);
        ButterKnife.bind(this);
        initUI();
    }

    private void initUI() {
        if (getLang(ChatHistoryActivity.this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right);
        } else {
            ivBack.setImageResource(R.mipmap.back_left);
        }
        sharedPrefManager = new SharedPrefManager(this);
        chatHistoryPresenter = new ChatHistoryPresenter(this, sharedPrefManager);
        chatHistoryPresenter.chat(rvMessages, pbPagination);
    }

}