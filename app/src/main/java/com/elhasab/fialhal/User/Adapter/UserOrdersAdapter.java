package com.elhasab.fialhal.User.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.AllOrdersModel.Datum;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.RequestDetails.View.RequestDetailsActivity;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Worker.WorkerOfferDeatailsActivity;
import com.elhasab.fialhal.Worker.WorkerRequestDetailsActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UserOrdersAdapter extends RecyclerView.Adapter<UserOrdersAdapter.ViewHolder> {
    ArrayList<com.elhasab.fialhal.Models.AllOrdersModel.Datum> homeList;
    Context context;
    LayoutInflater layoutInflater;
    ShimmerRecyclerView rvOrders;

    public UserOrdersAdapter(Context context, ShimmerRecyclerView rvOrders) {
        homeList = new ArrayList<>();
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rvOrders = rvOrders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = layoutInflater.from(context).inflate(R.layout.item_request, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (homeList.get(position).getImages().size() > 0) {
            ParentClass.LoadImageWithPicasso(homeList.get(position).getImages().get(0).getImage(), context, holder.ivOrderImage);
        } else {
            ParentClass.LoadImageWithPicasso(" ", context, holder.ivOrderImage);
        }
        holder.tvAddress.setText(homeList.get(position).getAddress());
        holder.tvDate.setText(homeList.get(position).getDate());
        holder.tvOrderTitle.setText(homeList.get(position).getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ParentClass.sharedPrefManager.getUserDate().getType().equals("worker")) {
                    Intent intent = new Intent(context, WorkerRequestDetailsActivity.class);
                    intent.putExtra("type", homeList.get(position).getStatus());
                    intent.putExtra("id", String.valueOf(homeList.get(position).getId()));
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, RequestDetailsActivity.class);
                    intent.putExtra("type", homeList.get(position).getStatus());
                    intent.putExtra("id", String.valueOf(homeList.get(position).getId()));
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (homeList.size() == 0) {
            rvOrders.showShimmerAdapter();
            return 10;
        } else {
            return homeList.size();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }

    public void addAll(List<Datum> data) {
        homeList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvOrderTitle)
        TextView tvOrderTitle;
        @BindView(R.id.ivOrderImage)
        ImageView ivOrderImage;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.tvDate)
        TextView tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(context, itemView);


        }
    }
}
