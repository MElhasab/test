package com.elhasab.fialhal.User.FollowingOrder.Presenter;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.w3c.dom.Text;

public interface FollowingOrderViewPresenter {
    void followingOrder(View viewSecondPhase, View viewThirdPhase, ImageView ivThirdPhase, ImageView ivForthPhase
            , TextView tvBasedOnStatus, TextView tvStatus,String id);

    void rate(String id, String rate, EditText etRate);

    void changeOrderStatus(String id, String status, BottomSheetDialog popAddRate);
}
