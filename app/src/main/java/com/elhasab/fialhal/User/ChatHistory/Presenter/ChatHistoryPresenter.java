package com.elhasab.fialhal.User.ChatHistory.Presenter;

import android.content.Context;
import android.widget.ProgressBar;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class ChatHistoryPresenter extends BasePresenter implements ChatHistoryViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public ChatHistoryPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void chat(ShimmerRecyclerView rvMessages, ProgressBar pbPagination) {

    }
}
