package com.elhasab.fialhal.Authentication.NewPassword.Presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class NewPasswordPresenter extends BasePresenter implements NewPasswordViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public NewPasswordPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }


    @Override
    public void newPass(String mobile, EditText etNewPassword, EditText etConfirmNewPass, String deviceToken) {
        Boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etNewPassword.getText().toString())) {
            etNewPassword.setError(context.getString(R.string.password));
            focusView = etNewPassword;
            cancel = true;
        }
        if (TextUtils.isEmpty(etConfirmNewPass.getText().toString())) {
            etConfirmNewPass.setError(context.getString(R.string.password));
            focusView = etConfirmNewPass;
            cancel = true;
        }
        if (!(etNewPassword.getText().toString()).equals(etConfirmNewPass.getText().toString())) {
            cancel = true;
            etNewPassword.setError(context.getString(R.string.password));
            etConfirmNewPass.setError(context.getString(R.string.password));
        }
        if (cancel) {

        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).change_password(mobile, etNewPassword.getText().toString()
                    , deviceToken, "android")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<UserModel> observer = new Observer<UserModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(UserModel userModel) {
                    stopLoading();
                    if (userModel != null) {
                        if (userModel.isValue()) {
                            sharedPrefManager.setUserDate(userModel.getData());
                            sharedPrefManager.setLoginStatus(true);
                            if (userModel.getData().getType().equals("client")) {
                                Intent intent = new Intent(context, HomeUserActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                                Bungee.split(context);
                            } else {
                                Intent intent = new Intent(context, HomeWorkerActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                                Bungee.split(context);
                            }
                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }
    }
}
