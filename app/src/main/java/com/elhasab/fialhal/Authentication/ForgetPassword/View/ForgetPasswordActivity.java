package com.elhasab.fialhal.Authentication.ForgetPassword.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.ForgetPassword.Presenter.ForgetPasswordPresenter;
import com.elhasab.fialhal.Authentication.Login.View.LoginActivity;
import com.elhasab.fialhal.Authentication.Register.View.RegisterActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;

public class ForgetPasswordActivity extends ParentClass {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.tvCountryCode)
    TextView tvCode;

    ForgetPasswordPresenter forgetPasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        if (getLang(this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right);
        } else {
            ivBack.setImageResource(R.mipmap.back_left);
        }
        if (getLang(this).equals("ar")) {
            tvCode.setText("+669");
        }
        forgetPasswordPresenter = new ForgetPasswordPresenter(this);
    }

    @OnClick(R.id.tvSend)
    void send() {
        forgetPasswordPresenter.sendPass(etMobile);
    }

    @OnClick(R.id.tvLogin)
    void login() {
        Intent intent = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        Bungee.split(ForgetPasswordActivity.this);
    }

    @OnClick(R.id.ivBack)
    void goBack() {
        finish();
    }


}