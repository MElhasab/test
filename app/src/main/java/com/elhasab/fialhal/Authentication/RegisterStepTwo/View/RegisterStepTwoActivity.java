package com.elhasab.fialhal.Authentication.RegisterStepTwo.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Authentication.RegisterStepTwo.Presenter.RegisterStepTwoPresenter;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class RegisterStepTwoActivity extends AppCompatActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvRegister)
    TextView tvRegister;
    @BindView(R.id.rlOneCategory)
    RelativeLayout rlOneCategory;
    @BindView(R.id.rlMoreCategory)
    RelativeLayout rlMoreCategory;
    @BindView(R.id.rvOneCategory)
    ShimmerRecyclerView rvOneCategory;
    @BindView(R.id.rvMoreCategory)
    ShimmerRecyclerView rvMoreCategory;
    @BindView(R.id.pbPagenation)
    ProgressBar pbPagination;
    String type = "single";

    RegisterStepTwoPresenter registerStepTwoPresenter;
    SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_step_two);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        sharedPrefManager = new SharedPrefManager(this);
        registerStepTwoPresenter = new RegisterStepTwoPresenter(this, sharedPrefManager);
        registerStepTwoPresenter.getCtegory(rvOneCategory, rvMoreCategory, type, tvRegister);
    }

    @OnClick(R.id.rlOneCategory)
    void rlOneCategory() {
        rlOneCategory.setBackgroundResource(R.drawable.drawable_button);
        rlMoreCategory.setBackgroundResource(R.color.lightButtonColor);
        rvOneCategory.setVisibility(View.VISIBLE);
        rvMoreCategory.setVisibility(View.GONE);
    }

    @OnClick(R.id.rlMoreCategory)
    void rlMoreCategory() {
        rlMoreCategory.setBackgroundResource(R.drawable.drawable_button);
        rlOneCategory.setBackgroundResource(R.color.lightButtonColor);
        rvMoreCategory.setVisibility(View.VISIBLE);
        rvOneCategory.setVisibility(View.GONE);
    }


}