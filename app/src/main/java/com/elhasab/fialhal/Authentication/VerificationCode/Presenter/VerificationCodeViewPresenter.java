package com.elhasab.fialhal.Authentication.VerificationCode.Presenter;

public interface VerificationCodeViewPresenter {
    void activateUser(String mobile,String code);
    void checkCode(String mobile,String code);

    void worker_finish_code(String order_id,String code);
}
