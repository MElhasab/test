package com.elhasab.fialhal.Authentication.NewPassword.Presenter;

import android.widget.EditText;

public interface NewPasswordViewPresenter {
    void newPass(String mobile, EditText etNewPassword, EditText etConfirmNewPass, String deviceToken);
}
