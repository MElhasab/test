package com.elhasab.fialhal.Authentication.RegisterStepTwo.Presenter;

import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

public interface RegisterStepTwoViewPresenter {
    void getCtegory(ShimmerRecyclerView rvOneCategory, ShimmerRecyclerView rvManyCategory, String type, TextView tvLogin);
}
