package com.elhasab.fialhal.Authentication.Login.Presenter;

import android.widget.EditText;

public interface LoginViewPresenter {
    void login(EditText etMobile, EditText etPassword, String deviceToken);
}
