package com.elhasab.fialhal.Authentication.Register.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.Login.View.LoginActivity;
import com.elhasab.fialhal.Authentication.Register.Presenter.RegisterPresenter;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;

public class RegisterActivity extends ParentClass implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        GoogleMap.OnMarkerClickListener {


    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvCountryCode)
    TextView tvCode;
    @BindView(R.id.rlExperience)
    RelativeLayout rlExperience;
    @BindView(R.id.etExperience)
    EditText etExperience;
    @BindView(R.id.rlUser)
    RelativeLayout rlUser;
    @BindView(R.id.rlTechnical)
    RelativeLayout rlWorker;
    @BindView(R.id.spCountries)
    Spinner spCountries;
    @BindView(R.id.spCities)
    Spinner spCities;

    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;


    public Boolean first = true;
    String lat = "";
    String lng = "";


    RegisterPresenter registerPresenter;
    SharedPrefManager sharedPrefManager;
    String deviceToken = "tokenToBeChanged";
    String type = "client";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        if (getLang(this).equals("ar")) {
            tvCode.setText("+669");
        }
        SharedPreferences preferences = getSharedPreferences(Constants.mobileToken, MODE_PRIVATE);
        if (!preferences.getString("m_token", "").equals("")) {
            deviceToken = preferences.getString("m_token", "");
        }
        if (!type.equals("client")) {
            rlExperience.setVisibility(View.VISIBLE);
        }
        sharedPrefManager = new SharedPrefManager(this);
        registerPresenter = new RegisterPresenter(this, sharedPrefManager);
        registerPresenter.getCountry(spCountries);
        registerPresenter.getCities(spCities);

    }

    @OnClick(R.id.rlUser)
    void rlUser() {
        rlUser.setBackgroundResource(R.drawable.drawable_button);
        rlWorker.setBackgroundResource(R.color.lightButtonColor);
    }

    @OnClick(R.id.rlTechnical)
    void rlWorker() {
        rlWorker.setBackgroundResource(R.drawable.drawable_button);
        rlUser.setBackgroundResource(R.color.lightButtonColor);
    }


    @OnClick(R.id.tvRegister)
    void register() {
        if (type.equals("client")) {
            registerPresenter.register(etUsername, etMobile, etEmail, etPassword, String.valueOf(registerPresenter.cityId), deviceToken
                    , type, String.valueOf(0), lat, lng);
        } else {
            registerPresenter.register(etUsername, etMobile, etEmail, etPassword, String.valueOf(registerPresenter.cityId), deviceToken
                    , type, etExperience.getText().toString(), lat, lng);
        }
    }

    @OnClick(R.id.tvLogin)
    void login() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        Bungee.split(RegisterActivity.this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, (com.google.android.gms.location.LocationListener) this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lat = String.valueOf(location.getLatitude());
        lng = String.valueOf(location.getLongitude());
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        client.connect();
    }
}