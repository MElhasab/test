package com.elhasab.fialhal.Authentication.NewPassword.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;

import com.elhasab.fialhal.Authentication.ForgetPassword.View.ForgetPasswordActivity;
import com.elhasab.fialhal.Authentication.Login.View.LoginActivity;
import com.elhasab.fialhal.Authentication.NewPassword.Presenter.NewPasswordPresenter;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class NewPasswordActivity extends ParentClass {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmNewPassword)
    EditText etConfirmPassword;

    String deviceToken = "tokenToBeChanged";
    SharedPrefManager sharedPrefManager;
    NewPasswordPresenter newPasswordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        ButterKnife.bind(this);

        intiUI();
    }

    public void intiUI() {
        SharedPreferences preferences = getSharedPreferences(Constants.mobileToken, MODE_PRIVATE);
        if (!preferences.getString("m_token", "").equals("")) {
            deviceToken = preferences.getString("m_token", "");
        }
        if (getLang(this).equals("ar")) {
            if (getLang(this).equals("ar")) {
                ivBack.setImageResource(R.mipmap.back_right);
            } else {
                ivBack.setImageResource(R.mipmap.back_left);
            }
        }
        sharedPrefManager = new SharedPrefManager(this);
        newPasswordPresenter = new NewPasswordPresenter(this, sharedPrefManager);
    }

    @OnClick(R.id.tvSend)
    void send() {
        newPasswordPresenter.newPass(getIntent().getStringExtra("mobile"), etNewPassword, etConfirmPassword, deviceToken);
    }

    @OnClick(R.id.tvLogin)
    void login() {
        Intent intent = new Intent(NewPasswordActivity.this, LoginActivity.class);
        startActivity(intent);
        Bungee.split(NewPasswordActivity.this);
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
    }
}