package com.elhasab.fialhal.Authentication.Login.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import spencerstudios.com.bungeelib.Bungee;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.ForgetPassword.View.ForgetPasswordActivity;
import com.elhasab.fialhal.Authentication.Login.Presenter.LoginPresenter;
import com.elhasab.fialhal.Authentication.Register.View.RegisterActivity;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;

public class LoginActivity extends ParentClass {

    @BindView(R.id.etMobile)
    EditText etMobile;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvCountryCode)
    TextView tvCode;

    LoginPresenter loginPresenter;
    SharedPrefManager sharedPrefManager;
    String deviceToken = "tokenToBeChanged";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    public void initUI() {
        if (getLang(this).equals("ar")) {
            tvCode.setText("+669");
        }
        dismiss_keyboard();
        SharedPreferences preferences = getSharedPreferences(Constants.mobileToken, MODE_PRIVATE);
        if (!preferences.getString("m_token", "").equals("")) {
            deviceToken = preferences.getString("m_token", "");
        }
        sharedPrefManager = new SharedPrefManager(this);
        loginPresenter = new LoginPresenter(this, sharedPrefManager);
    }

    @OnClick(R.id.tvLogin)
    void login() {
        loginPresenter.login(etMobile, etPassword, deviceToken);
    }

    @OnClick(R.id.tvForgetPassword)
    void forgetPass() {
        Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
        startActivity(intent);
        Bungee.split(LoginActivity.this);
    }

    @OnClick(R.id.tvRegister)
    void register() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
        Bungee.split(LoginActivity.this);
    }
}