package com.elhasab.fialhal.Authentication.ForgetPassword.Presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.elhasab.fialhal.Authentication.VerificationCode.View.VerificationCodeActivity;
import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class ForgetPasswordPresenter extends BasePresenter implements ForgetPasswordViewPresenter {

    Context context;

    public ForgetPasswordPresenter(Context context) {
        this.context = context;
    }

    @Override
    public void sendPass(EditText etMobile) {
        Boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            etMobile.setError(context.getString(R.string.mobileNum));
            focusView = etMobile;
            cancel = true;
        }
        if (cancel) {

        } else {
            stopLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).forget_password(etMobile.getText().toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<UserModel> observer = new Observer<UserModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(UserModel userModel) {
                    if (userModel != null) {
                        if (userModel.isValue()) {
                            Intent intent = new Intent(context, VerificationCodeActivity.class);
                            intent.putExtra("mobile", etMobile.getText().toString());
                            context.startActivity(intent);
                            Bungee.split(context);

                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }

                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }

    }
}
