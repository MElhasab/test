package com.elhasab.fialhal.Authentication.VerificationCode.Presenter;

import android.content.Context;
import android.content.Intent;

import com.elhasab.fialhal.Authentication.NewPassword.View.NewPasswordActivity;
import com.elhasab.fialhal.Models.SingleOrderModel.SingleOrderModel;
import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.Constants;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class VerificationCodePresenter extends BasePresenter implements VerificationCodeViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public VerificationCodePresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void checkCode(String mobile, String code) {
        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).check_forget_code(mobile, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<UserModel> observer = new Observer<UserModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UserModel userModel) {
                stopLoading();
                if (userModel != null) {
                    if (userModel.isValue()) {
                        sharedPrefManager.setUserDate(userModel.getData());
                        sharedPrefManager.setLoginStatus(true);
                        Intent intent = new Intent(context, NewPasswordActivity.class);
                        intent.putExtra("mobile", mobile);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        context.startActivity(intent);
                        Bungee.split(context);
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }
            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }

    @Override
    public void activateUser(String mobile, String code) {
        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).activate_user(mobile, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<UserModel> observer = new Observer<UserModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(UserModel userModel) {
                stopLoading();
                if (userModel != null) {
                    if (userModel.isValue()) {
                        sharedPrefManager.setUserDate(userModel.getData());
                        sharedPrefManager.setLoginStatus(true);
                        if (userModel.getData().getType().equals("worker")) {
                            Intent intent = new Intent(context, HomeWorkerActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);
                        } else {
                            Intent intent = new Intent(context, HomeUserActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            context.startActivity(intent);
                            Bungee.split(context);
                        }


                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                }

            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                stopLoading();
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
    }

    @Override
    public void worker_finish_code(String order_id, String code) {
        startLoading();
        Observable observable = RetroWeb.getClient().create(MainServices.class).worker_finish_code(
                Constants.beforApiToken +
                        sharedPrefManager.getUserDate().getToken(), order_id, code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<SingleOrderModel> observer = new Observer<SingleOrderModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(SingleOrderModel userModel) {
                stopLoading();
                if (userModel != null) {
                    if (userModel.isValue()) {
                        successToast(context, context.getString(R.string.codeEnterdSuccefully));
                        Intent intent = new Intent(context, HomeWorkerActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        context.startActivity(intent);
                        Bungee.split(context);
                    } else {
                        errorToast(context, userModel.getMsg());
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));

                }

            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                stopLoading();

            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);

    }
}
