package com.elhasab.fialhal.Authentication.VerificationCode.View;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.elhasab.fialhal.Authentication.VerificationCode.Presenter.VerificationCodePresenter;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.ParentClass;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.goodiebag.pinview.Pinview;

public class VerificationCodeActivity extends ParentClass {

    @BindView(R.id.tvWelcome)
    TextView tvWelcome;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    Pinview pinview;
    SharedPrefManager sharedPrefManager;
    VerificationCodePresenter verificationCodePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);
        ButterKnife.bind(this);
        initUI();
    }

    public void initUI() {
        if (getLang(this).equals("ar")) {
            ivBack.setImageResource(R.mipmap.back_right);
        } else {
            ivBack.setImageResource(R.mipmap.back_left);
        }
        sharedPrefManager = new SharedPrefManager(this);
        verificationCodePresenter = new VerificationCodePresenter(this, sharedPrefManager);
        if (getIntent().getStringExtra("type").equals("register")) {
            tvWelcome.setText(R.string.enterCode);
        }
        if (getIntent().getStringExtra("type").equals("forget")) {
            tvWelcome.setText(R.string.changePassword);
        }
        if (getIntent().getStringExtra("type").equals("order")) {
            tvWelcome.setText(R.string.enterCodeFromNotificationToFinishOrder);
        }
        pinview = new Pinview(this);
        pinview = findViewById(R.id.pinView);
    }

    @OnClick(R.id.tvSend)
    void send() {
        if (getIntent().getStringExtra("type").equals("register")) {
            verificationCodePresenter.activateUser(getIntent().getStringExtra("mobile"), pinview.getValue());
        }
        if (getIntent().getStringExtra("type").equals("forget")) {
            verificationCodePresenter.checkCode(getIntent().getStringExtra("mobile"), pinview.getValue());
        }
        if (getIntent().getStringExtra("type").equals("order")) {
            verificationCodePresenter.worker_finish_code(getIntent().getStringExtra("orderId"), pinview.getValue());
        }
    }

    @OnClick(R.id.ivBack)
    void back() {
        finish();
    }
}