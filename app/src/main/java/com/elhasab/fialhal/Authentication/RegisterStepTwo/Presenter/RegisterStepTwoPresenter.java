package com.elhasab.fialhal.Authentication.RegisterStepTwo.Presenter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.cooltechworks.views.shimmer.ShimmerRecyclerView;
import com.elhasab.fialhal.Models.CategoryModel.CategoryModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;
import com.elhasab.fialhal.Worker.Adapter.CheckBoxAdapter;
import com.elhasab.fialhal.Worker.Adapter.RadioBtnAdapter;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RegisterStepTwoPresenter extends BasePresenter implements RegisterStepTwoViewPresenter {
    Context context;
    SharedPrefManager sharedPrefManager;

    public RegisterStepTwoPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void getCtegory(ShimmerRecyclerView rvOneCategory, ShimmerRecyclerView rvManyCategory, String type, TextView tvLogin) {

        final RadioBtnAdapter adapter1 = new RadioBtnAdapter(context, rvOneCategory, sharedPrefManager, tvLogin, type);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvOneCategory.setLayoutManager(linearLayoutManager1);

        CheckBoxAdapter adapter2 = new CheckBoxAdapter(context, rvManyCategory, sharedPrefManager, tvLogin, type);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        rvManyCategory.setLayoutManager(linearLayoutManager2);

        Observable observable = RetroWeb.getClient().create(MainServices.class).categories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        Observer<CategoryModel> observer = new Observer<CategoryModel>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(CategoryModel categoryModel) {
                if (categoryModel != null) {
                    if (categoryModel.isValue()) {
                        rvOneCategory.hideShimmerAdapter();
                        rvManyCategory.hideShimmerAdapter();
                        adapter1.addAll(categoryModel.getData());
                        adapter1.notifyDataSetChanged();
                        adapter2.addAll(categoryModel.getData());
                        adapter2.notifyDataSetChanged();

                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                        rvOneCategory.setVisibility(View.GONE);
                        rvManyCategory.setVisibility(View.GONE);
                    }
                } else {
                    errorToast(context, context.getString(R.string.someThingWentWrong));
                    rvOneCategory.setVisibility(View.GONE);
                    rvManyCategory.setVisibility(View.GONE);
                }

            }

            @Override
            public void onError(Throwable e) {
                handleApiException(context, e);
                rvOneCategory.setVisibility(View.GONE);
                rvManyCategory.setVisibility(View.GONE);
            }

            @Override
            public void onComplete() {

            }
        };
        observable.subscribe(observer);
        rvOneCategory.setAdapter(adapter1);
        rvManyCategory.setAdapter(adapter2);
    }
}
