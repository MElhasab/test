package com.elhasab.fialhal.Authentication.ForgetPassword.Presenter;

import android.widget.EditText;

public interface ForgetPasswordViewPresenter {
    void sendPass(EditText etMobile);
}
