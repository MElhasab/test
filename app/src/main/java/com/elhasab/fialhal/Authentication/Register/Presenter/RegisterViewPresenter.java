package com.elhasab.fialhal.Authentication.Register.Presenter;

import android.widget.EditText;
import android.widget.Spinner;

public interface RegisterViewPresenter {
    void getCountry(Spinner spCountries);

    void getCities(Spinner spCities);

    void register(EditText username, EditText mobile, EditText email, EditText etPassword, String cityId,
                  String deviceToken, String type, String experience, String lat, String lng);
}
