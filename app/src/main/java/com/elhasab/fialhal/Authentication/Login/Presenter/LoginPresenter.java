package com.elhasab.fialhal.Authentication.Login.Presenter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Network.MainServices;
import com.elhasab.fialhal.R;
import com.elhasab.fialhal.Worker.HomeWorker.View.HomeWorkerActivity;
import com.elhasab.fialhal.User.HomeUser.View.HomeUserActivity;
import com.elhasab.fialhal.Utils.BasePresenter;
import com.elhasab.fialhal.Utils.RetroWeb;
import com.elhasab.fialhal.Utils.SharedPrefManager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import spencerstudios.com.bungeelib.Bungee;

public class LoginPresenter extends BasePresenter implements LoginViewPresenter {

    Context context;
    SharedPrefManager sharedPrefManager;

    public LoginPresenter(Context context, SharedPrefManager sharedPrefManager) {
        this.context = context;
        this.sharedPrefManager = sharedPrefManager;
    }

    @Override
    public void login(EditText etMobile, EditText etPassword, String deviceToken) {

        Boolean cancel = false;
        View focusView = null;
        if (TextUtils.isEmpty(etMobile.getText().toString())) {
            etMobile.setError(context.getString(R.string.mobileNum));
            focusView = etMobile;
            cancel = true;
        }
        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            etPassword.setError(context.getString(R.string.password));
            focusView = etPassword;
            cancel = true;
        }
        if (cancel) {

        } else {
            startLoading();
            Observable observable = RetroWeb.getClient().create(MainServices.class).login("669" + etMobile.getText().toString()
                    , etPassword.getText().toString(), deviceToken, "android", sharedPrefManager.getUserDate().getType()
                    , sharedPrefManager.getLat(), sharedPrefManager.getLng())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
            Observer<UserModel> observer = new Observer<UserModel>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(UserModel userModel) {
                    if (userModel != null) {
                        if (userModel.isValue()) {
                            sharedPrefManager.setUserDate(userModel.getData());
                            sharedPrefManager.setLoginStatus(true);
                            if (sharedPrefManager.getUserDate().getType().equals("client")) {
                                Intent intent = new Intent(context, HomeUserActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                                Bungee.split(context);
                            } else {
                                Intent intent = new Intent(context, HomeUserActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                context.startActivity(intent);
                                Bungee.split(context);
                            }
                        } else {
                            errorToast(context, context.getString(R.string.someThingWentWrong));
                        }
                    } else {
                        errorToast(context, context.getString(R.string.someThingWentWrong));
                    }

                }

                @Override
                public void onError(Throwable e) {
                    handleApiException(context, e);
                    stopLoading();
                }

                @Override
                public void onComplete() {

                }
            };
            observable.subscribe(observer);
        }

    }
}
