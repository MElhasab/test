package com.elhasab.fialhal.Network;

public class MainURL {

    public static final String register = "register";
    public static final String select_categories = "select_categories";
    public static final String login = "login";
    public static final String logout = "logout";
    public static final String forget_password = "forget_password";
    public static final String check_forget_code = "check_forget_code";
    public static final String change_password = "change_password";
    public static final String update_profile = "update_profile";
    public static final String change_old_password = "change_old_password";
    public static final String countries = "countries";
    public static final String cities = "cities/{country_id}";
    public static final String categories = "categories";
    public static final String sub_categories = "sub_categories/{category_id}";
    public static final String make_order = "order";
    public static final String single_order = "order/{id}";
    public static final String offer_response = "offer_response";
    public static final String pay_order = "pay_order";
    public static final String client_orders = "client_orders";
    public static final String cancel_order = "cancel_order/{id}";
    public static final String delete_order = "order/{id}";
    public static final String update_order = "update_order";
    public static final String client_and_worker_transfers = "client_transfers";
    public static final String chat_history = "chat_history";
    public static final String check_room = "check_room/{id}";
    public static final String send_message = "send_message";
    public static final String notifications = "notifications";
    public static final String settings = "settings";
    public static final String contact_us = "contact_us";


    public static final String worker_home = "worker_home";
    public static final String add_offer = "add_offer";
    public static final String worker_categories = "worker_categories";
    public static final String worker_orders = "worker_orders/{waiting}";
    public static final String change_order_status = "change_order_status";
    public static final String update_worker_categories = "update_categories";
    public static final String worker_finish_code = "worker_finish_code";
    public static final String rate = "rate";
    public static final String main_categories_and_its_sub = "main_categories";
    public static final String update_offer = "update_offer";
    public static final String delete_offer = "delete_offer/{id}";
    public static final String single_offer = "single_offer/{id}";
    public static final String categories_with_check = "categories_with_check";
    public static final String activate_user = "activate_user";
    public static final String request_transfer = "request_transfer";


}
