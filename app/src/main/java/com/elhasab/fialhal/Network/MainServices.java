package com.elhasab.fialhal.Network;

import com.elhasab.fialhal.Models.AllOrdersModel.AllOrdersModel;
import com.elhasab.fialhal.Models.CategoryModel.CategoryModel;
import com.elhasab.fialhal.Models.CategoryWithCheck.CategoryWithCheckModel;
import com.elhasab.fialhal.Models.ChatHistoryModel.ChatHistoryModel;
import com.elhasab.fialhal.Models.CheckRoomModel.CheckRoomModel;
import com.elhasab.fialhal.Models.CitiesModel.CitiesModel;
import com.elhasab.fialhal.Models.ClientAndWorkerTransfer.ClientAndWorkerTranferModel;
import com.elhasab.fialhal.Models.ClientOrdersModel.ClientOrdersModel;
import com.elhasab.fialhal.Models.CountriesModel.CountriesModel;
import com.elhasab.fialhal.Models.GeneralResponse;
import com.elhasab.fialhal.Models.MainCategoryAndItsSub.MainCategoryAndItsSubModel;
import com.elhasab.fialhal.Models.NotificationsModels.NotificationsModel;
import com.elhasab.fialhal.Models.OfferModel.OfferModel;
import com.elhasab.fialhal.Models.OrderModel.OrderModel;
import com.elhasab.fialhal.Models.SettingModel.SettingModel;
import com.elhasab.fialhal.Models.SubCategoryModel.SubCategoryModel;
import com.elhasab.fialhal.Models.UserModel.UserModel;
import com.elhasab.fialhal.Models.WorkerCategoryAfterRegister.WorkerCategoryAfterRegisterModel;
import com.elhasab.fialhal.Models.WorkerCategoryModel.WorkerCategoryModel;
import com.elhasab.fialhal.Models.WorkerHomeModel.WorkerHomeModel;
import com.elhasab.fialhal.Models.WorkerOrderModel.WorkerOrderModel;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface MainServices {

    @FormUrlEncoded
    @POST(MainURL.register)
    Observable<UserModel> register(
            @Field("username") String username,
            @Field("mobile") String mobile,
            @Field("email") String email,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("city_id") String city_id,
            @Field("type") String type,
            @Field("experience") String experience,
            @Field("lat") String lat,
            @Field("lng") String lng
    );

    @FormUrlEncoded
    @POST(MainURL.select_categories)
    Observable<WorkerCategoryAfterRegisterModel> select_categories(
            @Header("Authorization") String Authorization,
            @Field("category_type") String category_type,
            @Field("categories") String categories
    );

    @FormUrlEncoded
    @POST(MainURL.login)
    Observable<UserModel> login(
            @Field("mobile") String mobile,
            @Field("password") String password,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type,
            @Field("type") String type,
            @Field("lat") String lat,
            @Field("lng") String lng
    );

    @FormUrlEncoded
    @POST(MainURL.logout)
    Observable<UserModel> logout(
            @Header("Authorization") String Authorization,
            @Field("device_token") String device_token

    );

    @FormUrlEncoded
    @POST(MainURL.forget_password)
    Observable<UserModel> forget_password(
            @Field("mobile") String mobile
    );

    @FormUrlEncoded
    @POST(MainURL.check_forget_code)
    Observable<UserModel> check_forget_code(
            @Field("mobile") String mobile,
            @Field("code") String code
    );

    @FormUrlEncoded
    @POST(MainURL.change_password)
    Observable<UserModel> change_password(
            @Field("mobile") String mobile,
            @Field("password") String password,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );

    @FormUrlEncoded
    @POST(MainURL.update_profile)
    Observable<UserModel> updateProfileWithoutImage(
            @Header("Authorization") String Authorization,
            @Field("username") String username,
            @Field("email") String email,
            @Field("mobile") String mobile,
            @Field("city_id") String city_id,
            @Field("experience") String experience
    );

    @FormUrlEncoded
    @POST(MainURL.update_profile)
    Observable<UserModel> updateProfileWithImage(
            @Header("Authorization") String Authorization,
            @Query("username") String username,
            @Query("email") String email,
            @Query("mobile") String mobile,
            @Query("city_id") String city_id,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST(MainURL.change_old_password)
    Observable<UserModel> change_old_password(
            @Header("Authorization") String Authorization,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password,
            @Field("new_password_confirmation") String new_password_confirmation
    );

    @GET(MainURL.countries)
    Observable<CountriesModel> countries();

    @GET(MainURL.cities)
    Observable<CitiesModel> cities();

    @GET(MainURL.categories)
    Observable<CategoryModel> categories();

    @GET(MainURL.sub_categories)
    Observable<SubCategoryModel> sub_categories(
            @Path("id") String id
    );

    @Multipart
    @POST(MainURL.make_order)
    Observable<OrderModel> make_order(
            @Header("Authorization") String Authorization,
            @Query("categories") String categories,
            @Query("title") String title,
            @Query("description") String description,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("address") String address,
            @Query("date") String date,
            @Query("start_time") String start_time,
            @Query("finish_time") String finish_time,
            @Part List<MultipartBody.Part> image
    );

    @GET(MainURL.single_order)
    Observable<OrderModel> single_orders(
            @Header("Authorization") String Authorization,
            @Path("id") String id
    );

    @GET()
    Observable<AllOrdersModel> orders(
            @Header("Authorization") String Authorization,
            @Url String url
    );

    @FormUrlEncoded
    @POST(MainURL.offer_response)
    Observable<OrderModel> offer_response(
            @Header("Authorization") String Authorization,
            @Field("offer_id") String offer_id,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST(MainURL.pay_order)
    Observable<OrderModel> pay_order(
            @Header("Authorization") String Authorization,
            @Field("order_id") String order_id,
            @Field("payment_type") String payment_type
    );

    @GET(MainURL.client_orders)
    Observable<ClientOrdersModel> client_orders(
            @Header("Authorization") String Authorization
    );

    @GET(MainURL.cancel_order)
    Observable<GeneralResponse> cancel_order(
            @Header("Authorization") String Authorization,
            @Path("id") String id
    );

    @DELETE(MainURL.delete_order)
    Observable<GeneralResponse> delete_order(
            @Header("Authorization") String Authorization,
            @Path("id") String id
    );

    @FormUrlEncoded
    @POST(MainURL.update_order)
    Observable<OrderModel> updateOrderWithoutImage(
            @Header("Authorization") String Authorization,
            @Field("title") String title,
            @Field("description") String description,
            @Field("city_id") String city_id,
            @Field("lat") String lat,
            @Field("lng") String lng,
            @Field("address") String address,
            @Field("date") String date,
            @Field("start_time") String start_time,
            @Field("finish_time") String finish_time,
            @Field("order_id") String order_id
    );

    @FormUrlEncoded
    @POST(MainURL.update_order)
    Observable<OrderModel> updateOrderWithImage(
            @Header("Authorization") String Authorization,
            @Query("title") String title,
            @Query("description") String description,
            @Query("city_id") String city_id,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("address") String address,
            @Query("date") String date,
            @Query("start_time") String start_time,
            @Query("finish_time") String finish_time,
            @Query("order_id") String order_id,
            @Part MultipartBody.Part image
    );

    @GET(MainURL.client_and_worker_transfers)
    Observable<ClientAndWorkerTranferModel> client_and_worker_transfers(
            @Header("Authorization") String Authorization,
            @Url String url
    );

    @GET(MainURL.chat_history)
    Observable<ChatHistoryModel> chat_history(
            @Header("Authorization") String Authorization
    );

    @GET(MainURL.check_room)
    Observable<CheckRoomModel> check_room(
            @Header("Authorization") String Authorization
    );

    @FormUrlEncoded
    @POST(MainURL.send_message)
    Observable<ChatHistoryModel> send_message(
            @Header("Authorization") String Authorization,
            @Field("room_id") String room_id,
            @Field("receiver_id") String receiver_id,
            @Field("message") String message
    );

    @GET(MainURL.notifications)
    Observable<NotificationsModel> notifications(
            @Header("Authorization") String Authorization,
            @Url String url
    );

    @GET(MainURL.settings)
    Observable<SettingModel> settings();

    @FormUrlEncoded
    @POST(MainURL.contact_us)
    Observable<GeneralResponse> contact_us(
            @Field("name") String name,
            @Field("mobile") String mobile,
            @Field("email") String email,
            @Field("message") String message
    );

    @GET(MainURL.worker_home)
    Observable<WorkerHomeModel> worker_home(
            @Url String url
    );

    @FormUrlEncoded
    @POST(MainURL.add_offer)
    Observable<OrderModel> add_offer(
            @Header("Authorization") String Authorization,
            @Field("price") String price,
            @Field("comment") String comment,
            @Field("order_id") String order_id
    );

    @GET(MainURL.worker_categories)
    Observable<WorkerCategoryModel> worker_categories(
            @Header("Authorization") String Authorization
    );

    @GET(MainURL.worker_orders)
    Observable<WorkerOrderModel> worker_orders(
            @Header("Authorization") String Authorization,
            @Url String url
    );

    @FormUrlEncoded
    @POST(MainURL.change_order_status)
    Observable<GeneralResponse> change_order_status(
            @Header("Authorization") String Authorization,
            @Field("order_id") String order_id,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST(MainURL.update_worker_categories)
    Observable<WorkerCategoryAfterRegisterModel> update_worker_categories(
            @Header("Authorization") String Authorization,
            @Field("category_type") String category_type,
            @Field("categories") String categories
    );

    @FormUrlEncoded
    @POST(MainURL.worker_finish_code)
    Observable<GeneralResponse> worker_finish_code(
            @Header("Authorization") String Authorization,
            @Field("order_id") String order_id,
            @Field("code") String code
    );

    @FormUrlEncoded
    @POST(MainURL.rate)
    Observable<GeneralResponse> rate(
            @Header("Authorization") String Authorization,
            @Field("order_id") String order_id,
            @Field("rate") String rate,
            @Field("comment") String comment
    );

    @GET(MainURL.main_categories_and_its_sub)
    Observable<MainCategoryAndItsSubModel> main_categories_and_its_sub();

    @FormUrlEncoded
    @POST(MainURL.update_offer)
    Observable<OfferModel> update_offer(
            @Header("Authorization") String Authorization,
            @Field("price") String price,
            @Field("comment") String comment,
            @Field("offer_id") String offer_id
    );

    @GET(MainURL.delete_offer)
    Observable<GeneralResponse> delete_offer(
            @Header("Authorization") String Authorization
    );

    @GET(MainURL.single_offer)
    Observable<OfferModel> single_offer(
            @Header("Authorization") String Authorization,
            @Path("id") String id
    );

    @GET(MainURL.categories_with_check)
    Observable<CategoryWithCheckModel> categories_with_check(
            @Header("Authorization") String Authorization
    );

    @FormUrlEncoded
    @POST(MainURL.activate_user)
    Observable<GeneralResponse> activate_user(
            @Field("mobile") String mobile,
            @Field("code") String code
    );

    @FormUrlEncoded
    @POST(MainURL.request_transfer)
    Observable<GeneralResponse> requestTransfer(
            @Header("Authorization") String Authorization,
            @Field("amount") String amount
    );

}
